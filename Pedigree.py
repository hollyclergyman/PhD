#!/usr/bin/env python3

import numpy as np
import pandas as pd
import os
import re
import gc
import itertools
from functools import partial
import multiprocessing as mp
import requests
from PopulationParameters import PopulationParameters
from MultiProcessDivision.divide import divide


class Pedigree():
    """
    This class is more of a helper class for the Phenotype_data_corrections() class. Its main 
    task is the data processing to enable dmu support.
    Further processing, especially of the results of eartag processing happens in the
    Phenotype_data_corrections() class. 
    """
    def __init__(self):
        self.pp = PopulationParameters()
        self.pedigree_folder = self.pp.pedigree_folder
        pattern = re.compile(r"Pedigree\ [aA-zZ\ ]*[A-Z]+.csv$")
        csv_files = [
            x for x in filter(pattern.match, self.pp.pedigree_csv_files)
        ]
        # solution kindly provided by:
        # https://stackoverflow.com/questions/3640359/regular-expressions-search-in-list/3640376#3640376
        self.csv_files = [x for x in csv_files if "adapted" not in x]
                
    def pedigree_eartags(self):
        d = {}
        for file in self.csv_files:
            f = pd.read_csv(f"{self.pedigree_folder}/{file}")
            _intermediate = []
            with mp.Pool(processes=mp.cpu_count()) as pool:
                _res = pool.map(self.eartag_country, f["Ear tag"], 8)
                _intermediate.append(_res)
            d[file] = np.unique(_intermediate)
        return d

    def eartag_country(self, x):
        val = str(x)
        try:
            if len(val) >= 15:
                return str(x)[:3]
            else:
                return 0
        except TypeError:
            return 0
    
    def __get_countries(self):
        header = {"user-agent":"Mozilla/5.0 (X11; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0"}
        try:
            r = requests.get(
                "https://raw.githubusercontent.com/lukes/ISO-3166-Countries-with-Regional-Codes/master/slim-2/slim-2.json",
                headers=header,
                verify="/etc/ssl/ca-bundle.pem"
            )
        except requests.exceptions.RequestException:
            return None
        countries = pd.DataFrame.from_dict(r.json())
        countries.index = countries["country-code"]
        countries.drop("country-code", inplace=True, axis="columns")
        return countries
        
    def countries(self, save=False, enforce=False):
        files = [
            x for x in os.listdir(self.pedigree_folder) if ".csv" in x and ".~lock." not in x
        ]
        if "country_encoding.csv" not in files or enforce is True:
            _gc = self.__get_countries()
            ped = self.pedigree_eartags()
            res = pd.DataFrame(
                columns=["Countries", "iso-3166"], 
                index=np.unique(np.concatenate([_i for _i in ped.values()]))
            )
            for _country in res.index:
                try:
                    if "GB" in _gc.loc[_country, "alpha-2"]:
                        res.loc[_country, "iso-3166"] = "UK"
                    else:
                        res.loc[_country, "iso-3166"] = _gc.loc[_country, "alpha-2"]
                    res.loc[_country, "Countries"] = _gc.loc[_country, "name"]
                except KeyError:
                    if _country == np.int(0):
                        res.loc[_country, "Countries"] = "Unknown"
                        res.loc[_country, "iso-3166"] = 0
            res["index values"] = list(range(len(res.index)))
            if save is True:
                res.to_csv(f"{self.pedigree_folder}/country_encoding.csv")
        else:
            res = pd.read_csv(f"{self.pedigree_folder}/country_encoding.csv", index_col=0)
            res["iso-3166"] = res["iso-3166"].apply(str)
        return res

    def convert_char_to_numeric(self, data, columns):
        """
        This function is not called by any other function in this class, nevertheless, 
        it fits this class thematically. Therefore, it is still in this class, even though
        the whole setting might seem strange. Currently this function is mainly used 
        to process data from the SheepData() class, where the ear tags contain the country
        code as character values.
        """
        divide = divide(data)
        res = pd.DataFrame()
        self.__countries = self.countries()
        with mp.Pool(mp.cpu_count()) as pool:
            to_execute = partial(self.char_adaption, columns)
            r = [v for v in pool.map(to_execute, divide) if v is not None]
            r = pd.concat(r, axis=0, sort=False)
            res = pd.concat([res, r], axis=0, sort=False)
        return res

    def char_adaption(self, columns, data):
        data[columns] = data[columns].apply(
            lambda x: "" if self.__replace_chars(x) is None else self.__replace_chars(x), 
            axis=1
        )
        return data
        
    def __replace_chars(self, data):
        d = pd.Series(index=data.index, name=data.name)
        for v in data.index:
            _val = str(data.loc[v])
            country = _val[:2]
            animal = _val[2:]
            try:
                _country = self.__countries[self.__countries["iso-3166"].str.match(country)]
                full_ear_tag = np.int64(str(np.unique(_country.index)[0]) + animal)
                d.loc[v] = full_ear_tag
            except KeyError:
                print(animal)
                continue
        return d
    
    def pedigree_adaption(self, saving=False):
        res = {}
        self.country_encoding = pd.read_csv(
            f"{self.pedigree_folder}/country_encoding.csv", index_col=0
        )
        for file in self.csv_files:
            d = pd.read_csv(
                f"{self.pedigree_folder}/{file}"
            )
            _data = self.eartag_correction(d, "animal_eartag")
            if _data is None:
                continue
            else:
                res[file] = _data
                del _data
                gc.collect()
            if saving is True:
                name = file.split(".")[0]
                res[file].to_csv(f"{self.pedigree_folder}/{name} Ear tag adapted.csv")
        return res
        
    def eartag_correction(self, data, process):
        split = divide(data)
        if not hasattr(Pedigree, 'country_encoding'):
            # checks if the variable named in the second step exists in the namespace 
            # of the class
            # solution from:
            # https://stackoverflow.com/questions/610883/how-to-know-if-an-object-has-an-attribute-in-python/610893#610893
            self.country_encoding = pd.read_csv(
                f"{self.pedigree_folder}/country_encoding.csv", index_col=0
            )
        """
        The workflow of this software is the following:
        - Each file is read at the beginning and passed to the overall controller function, 
        which is Pedigree().eartag_correction()
        - Here, the dataframe is split into several chunks, to distribute the workload evenly 
        between the different CPU cores. This is done in the function PopulationParameters().divide_df(), which 
        introduces a row based scheme on the DataFrame. The return of this function is a list, which
        suits well to the requirements to pass iterables to multiprocessing functions.
        - Since contextual multiprocess management is easier and ensures that all necessary steps are
        taken to avoid congruent writing on the same memory space, the multiprocessing is introduced as a
        contextual pool of workers. This pool closes implicitly after the function finishes.
        - Each of the induced worker threads starts an apply on the chunk it received
        - This threads now only work on the series of the former index, which is the ear tag values for 
        all the animals involved
        - The final function, adapting the ear tag to a shortened version only takes values into account, 
        which are at least 15 digits long, which is the ISO standard for ear tags in cattle. The 3 digits, 
        indicating the country at the beginning are replaced by a 1 or 2 digits no, which is registered in 
        a separate table.
        Reading of these ear tags therefore has to happen from the back to the front, since the length can 
        deviate depending on the country's index position in the translation table.
        """
        with mp.Pool(processes=mp.cpu_count()) as pool:
            """
            The core reason to define the same function three times with only deviations 
            in the column names passed to the function is the fact that parameter passing 
            with mp.pool() functions is more difficult and less readable than defining 
            the same function again.
            """
            to_execute = partial(self.adapting_function, process)
            res = [v for v in pool.map(to_execute, split) if v is not None]
            if len(res) > 0:
                _data = pd.concat(
                    res, 
                    axis=0
                )
        if '_data' in locals():
            return _data
        else:
            return None
    
    def adapting_function(self, process, data):
        if "animal_eartag" in process:
            data["Ear tag modified"] = data["Ear tag"].apply(lambda x: self.__working_function(x))
            # this has to be axis 1, to allow application of the function to 
            # each of the rows
            return data
        elif "parents_eartags" in process:
            data["Ear tag sire adapted"] = data["Ear tag sire"].apply(lambda x: self.__working_function(x))
            data["Ear tag dam adapted"] = data["Ear tag dam"].apply(lambda x: self.__working_function(x))
            return data
        elif "milk_recording" in process:
            data["ISO_LEBENSNR modified"] = data["ISO_LEBENSNR"].apply(
                lambda x: self.__working_function(x)
            )
            return data
        elif "blupf90" in process or "bf90_renum" in process:
            data.reset_index(inplace=True, drop=False)
            data["Ear tag"] = data["Ear tag"].apply(
                lambda x: self.__working_function(x)
            )
            # This step of removing animals without any records from the data set
            # is bound to minimize the amount of animals with missing records
            data = data[data["Ear tag"] != 0]
            # This step of changing the index follows the purpose to allow setting the 
            # results of sire and dam processing to a dataframe with the ear tags as 
            # index values
            data.index = data["Ear tag"]
            data.drop("Ear tag", axis=1, inplace=True)
            if "blupf90" in process:
                res = pd.DataFrame(
                    index=data.index,
                    columns=["Sire", "Dam", "Parent effect"]
                )
            else:
                res = pd.DataFrame(
                    index=data.index,
                    columns=["Sire", "Dam", "Alternate Dam", "Year of Birth"]
                )
            res["Sire"] = data["Ear tag sire"].apply(
                lambda x: self.__working_function(x)
            )
            res["Dam"] = data["Ear tag dam"].apply(lambda x: self.__working_function(x))
            if "blupf90" in process:
                res["Parent effect"] = np.repeat(1, len(res.index))
                # The default in this setup is to set the parents to exist both
                res.loc[res["Sire"] == 0,"Parent effect"] = 2
                res.loc[res["Dam"] == 0,"Parent effect"] = 2
                res.loc[(res["Dam"] == 0) & (res["Sire"] == 0), "Parent effect"] = 3
                # Adding the column of "Parent effect" allow sto use the additive animal
                # effect parameter in BlupF90, which is dependent on the amount of parents
                # specified in the file for the random effects, which the parents are to the
                # the respective animal.
                # Ressource:
                # Manual for BLUPF90 family of programs, page 12
                return res
            if "bf90_renum" in process:
                res["Alternate Dam"] = np.repeat(0, len(res.index))
                res["Year of Birth"] = data["Year of Birth"]
                res.fillna(0, inplace=True)
                return res
        else:
            return None

    def __working_function(self, data):
        if len(str(data)) >= 15:
            # as Fortran only accepts numbers up to a lenght of 14 digits, 
            # a conversion of the leading ISO-3166 encoding is necessary
            country_code = np.int(str(data)[:3])
            try:
                i_value = str(
                    self.country_encoding.loc[country_code, "index values"]
                )
            except KeyError:
                return 0
            try:
                return i_value + str(data)[3:]
            except IndexError:
                return 0
        else:
            return data
            """
            There was some confusion about old ear tag values, which were dropped in the
            following process. To avoid such confusion and to improve compatibility with 
            old ear tag formats, this has been changed, to return the data without any 
            changes.
            """
    
    def parent_eartag_adaption(self, saving=False):
        pattern = re.compile(r"^Pedigree\s[aA-zZ\s]+\ Ear\ tag\ adapted.csv$")
        files = [x for x in filter(pattern.match, os.listdir(self.pedigree_folder))]
        res = {}
        self.country_encoding = pd.read_csv(
            f"{self.pedigree_folder}/country_encoding.csv", index_col=0
        )
        for file in files:
            data = pd.read_csv(f"{self.pedigree_folder}/{file}", index_col=0)
            data.drop("index", inplace=True, axis="columns")
            name = file.split(".")[0]
            _data = self.eartag_correction(data, "parents_eartags")
            if _data is None:
                continue
            else:
                res[name] = _data
                del _data
                gc.collect()
            if saving is True:
                res[name].to_csv(f"{self.pedigree_folder}/{name} parents.csv")
                """
                The order of this saved data set is:
                Sire[1] and Dam[2]
                """
        if "res" in locals():
            return res
        else:
            return None
    
    def pedigree_for_dmu(self):
        pattern = re.compile(r"^Pedigree\s[aA-zZ\s]+\ Ear\ tag\ adapted\ parents.csv$")
        files = [x for x in filter(pattern.match, os.listdir(self.pedigree_folder))]
        for file in files:
            match = re.findall(r"^Pedigree\ [\D]*\ [\D]{3}\ [aA-zZ0-9\s]*", file)
            if len(match) > 0:
                pattern2 = re.compile(r"\s[\D]*\ [aA-zZ]{3}\s")
                name = re.findall(pattern2, match[0])[0]
                name = "_".join(name.strip().split(" ")[:-2])
            d = pd.read_csv(f"{self.pedigree_folder}/{file}", index_col=0)
            d = d.loc[:,
                [
                    "Ear tag modified", 
                    "Ear tag sire adapted", 
                    "Ear tag dam adapted", 
                    "Year of Birth"
                ]].astype(np.int64)
            """
            The order of columns specified in the selection statement is kept also in the resulting file.
            Animals' eartags, which form the rows in the resulting dataframe are not reordered throughout 
            this process. Therefore, the original order of pedigrees, as returned by the vit, is retained 
            throughout the whole process.
            """
            d.to_csv(f"{self.pedigree_folder}/pedigree_{name}", sep=" ", 
                header=False,  # header and index must be suppressed, as otherwise the 
                index=False,  # dmu program assumes they are part of the pedigree
                encoding="ascii"
            )
    
    def make_animals_unique(self, data):
        r = pd.DataFrame(
            index=np.unique(data["Ear tag"]),
            columns=[x for x in data.columns if "Ear tag" not in x]
        )
        for animal, dataset in data.groupby("Ear tag"):
            """
            The purpose of this loop is to remove duplicate animals, 
            since multiple files, such as for genotyped and for all animals
            are read in this function. To remove animals occuring in both 
            files, the following loop checks for duplicates in these animals.
            To keep the data thread safe, there is no multiprocessing applied 
            in this step.
            """
            for c in dataset.columns:
                if "Ear tag" in c:
                    continue
                if len(dataset.index) > 1:
                    _c = dataset.loc[:,c].to_numpy()
                    if (_c[0] == _c).all(0):
                        # This expression checks, whether all values assigned 
                        # to the index value in the series _c are the same.
                        # The pd.Series.all() mainly does this comparison, as 
                        # stated in the documentation:
                        # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.all.html
                        r.loc[animal, c] = np.int64(np.float64(_c[0]))
                    else:
                        r.loc[animal, c] = _c[0]
                        # In the highly unlikely case of different parents in the 
                        # dataset, the first entry is considered the true one
                if len(dataset.index) == 1:
                    r.loc[animal, c] = np.int64(
                        np.float64(dataset.loc[:,c].to_numpy()[0])
                    )
        return r


    def formatForBlupF90(self, save=False, read=False, include_typed=False):
        """
        This function serves the purpose to link existing functions to produce output 
        suitable to BlupF90. As the interface for obtaining of pedigree data already exists
        and it is safer to read data from there than from a file saved temporarily in the 
        respective directory, the data is read directly from the PopulationParameters() class
        if possible. 
        """
        ped = pd.DataFrame()
        self.country_encoding = pd.read_csv(
            f"{self.pedigree_folder}/country_encoding.csv", 
            index_col=0
        )
        breed_pattern = re.compile(r"[A-Z]{3}")
        if read is False:
            _data = self.pp.pedigree_data()
            if include_typed is False:
                pattern = re.compile(r"^Pedigree\ [A-Z]+$")
                read_data = {
                    re.findall(breed_pattern, x)[0]:_data[x] for x in _data.keys() 
                    if pattern.match(x) and re.findall(breed_pattern, x)
                }
            else:
                renum = pd.DataFrame()
                direct = pd.DataFrame()
                read_data = {}
                for _breed in set(
                    [re.findall(breed_pattern, v)[0] for v in _data.keys() 
                    if re.findall(breed_pattern, v)]):
                    pattern = re.compile(
                        r"^Pedigree\ (Typed\ %s|%s)$"%(re.escape(_breed), re.escape(_breed))
                    )
                    read_data[_breed] = [
                        _data[x] for x in _data.keys() if pattern.match(x)
                    ]
        else:
            if include_typed is False:
                pattern = re.compile(r"^Pedigree\ [A-Z]+.csv$")
                read_data = {
                    re.findall(breed_pattern, file)[0]: pd.read_csv(
                        f"{self.pedigree_folder}/{file}", 
                        index_col=0
                    ) for file in self.pp.pedigree_csv_files if pattern.match(file)
                    and re.findall(breed_pattern, file)
                }
            else:
                renum = pd.DataFrame()
                direct = pd.DataFrame()
                read_data = {}
                for _breed in set(
                    [re.findall(breed_pattern, v)[0] for v in self.pp.pedigree_csv_files 
                    if re.findall(breed_pattern, v)]):
                    pattern = re.compile(
                        r"^Pedigree\ (Typed\ %s|%s).csv$"%(re.escape(_breed), re.escape(_breed))
                    )
                    read_data[_breed] = [
                        pd.read_csv(
                            f"{self.pedigree_folder}/{file}", 
                            index_col=0
                        ) for file in self.pp.pedigree_csv_files if pattern.match(file)
                    ]
        r = {}
        for breed, table in read_data.items():
            _renum = self.eartag_correction(
                    table, "bf90_renum"
                ).astype(np.float64).astype(np.int64)
            _direct = self.eartag_correction(
                table, "blupf90"
            ).astype(np.float64).astype(np.int64)
            if include_typed is False and save is True and _renum is not None:
                _renum.to_csv(
                    f"{self.pedigree_folder}/Pedigree{breed}renum", 
                    sep=" ",
                    header=False
                )
            if include_typed is False and save is True and _direct is not None:
                _direct.to_csv(
                    f"{self.pedigree_folder}/Pedigree{breed}.ped", 
                    sep=" ",
                    header=False
                )
            if _renum is not None and _direct is not None:
                r[breed] = [_renum, _direct]
            if include_typed is True:
                _renum.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat(breed, len(_renum.index)),
                        _renum.index
                    ], names=["Breed", "Ear tag"]
                )
                _direct.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat(breed, len(_direct.index)),
                        _direct.index
                    ], names=["Breed", "Ear tag"]
                )
                renum = pd.concat([ped, _renum], axis=0)
                direct = pd.concat([direct, _direct], axis=0)
        if include_typed is True:
            res_renum = pd.DataFrame()
            res_direct = pd.DataFrame()
            for breed in np.unique(renum.index.get_level_values("Breed")):
                renum_data = renum.loc[pd.IndexSlice[breed,:],:]
                renum_data.reset_index(inplace=True, drop=False)
                renum_data.drop("Breed", axis=1, inplace=True)
                direct_data = direct.loc[pd.IndexSlice[breed,:],:]
                direct_data.reset_index(inplace=True, drop=False)
                direct_data.drop("Breed", axis=1, inplace=True)
                direct_unique = self.make_animals_unique(data=direct_data)
                direct_unique.fillna(0, inplace=True)
                direct_unique.loc[:,["Dam", "Sire"]] = direct_unique.loc[
                    :,["Dam", "Sire"]
                    ].astype(np.int64)
                
                renum_unique = self.make_animals_unique(data=renum_data)
                renum_unique.fillna(0, inplace=True)
                renum_unique.loc[:,["Dam", "Sire", "Alternate Dam", "Year of Birth"]] = _renum.loc[
                    :,["Dam", "Sire", "Alternate Dam", "Year of Birth"]
                    ].astype(np.int64)
                renum_unique = renum_unique[["Sire", "Dam", "Alternate Dam", "Year of Birth"]]
                if save is True:
                    direct_unique.to_csv(
                        f"{self.pedigree_folder}/Pedigree{breed}.ped", 
                        sep=" ", 
                        header=False
                    )
                    renum_unique.to_csv(
                        f"{self.pedigree_folder}/Pedigree{breed}renum", 
                        sep=" ",
                        header=False
                    )
                renum_unique.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat(breed, len(renum_unique.index)), 
                        renum_unique.index
                    ],
                    names=["Breed", "Ear tag"]    
                )
                direct_unique.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat(breed, len(direct_unique.index)), 
                        direct_unique.index
                    ],
                    names=["Breed", "Ear tag"]
                )
                res_renum = pd.concat([res_renum, renum_unique], axis=0)
                res_direct = pd.concat([res_direct, direct_unique], axis=0)
            return [res_renum, res_direct]
        else:
            return r

if __name__ == "__main__":
    d = Pedigree().formatForBlupF90(save=True)


# from astropy.io import ascii

# path = "/home/thomas/Downloads/Programme/dmuv6/R5.3-EM64T/examples/dmut2/"
# solutions_file = f"{path}test4.SOL"
# inbreed_file = f"{path}test4.INBREED"
# d_solution = ascii.read(solutions_file).to_pandas()
# d_solution.columns = [
#     "Effect type", 
#     "Trait no.", 
#     "Random effect no.", 
#     "Submodel Effect no.", 
#     "Class code", 
#     "No. of Observations", 
#     "Consecutive class no.", 
#     "Estimate", 
#     "Standard error of estimation"
# ]
# d_inbreed = ascii.read(inbreed_file).to_pandas()
# d_inbreed.columns = [
#     "ID", 
#     "No. of records", 
#     "Running no.", 
#     "Birth date", 
#     "Relative 1", 
#     "Relative 2", 
#     "No. of direct descendants", 
#     "Inbreeding Coefficient", 
#     "Diagonal element in L"
# ]
# d_solution

