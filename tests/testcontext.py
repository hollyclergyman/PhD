#!/usr/bin/env python3 

import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
"""
Kenneth Reitz describes this way to import dependencies from parent 
directories in the chapter about code organization in respositories.
Since this approach currently seems like the most straighforward way to
deal with such imports, it has been chosen here.
Reference:
Hitchhiker's guide to Python, page 38
"""

from PopulationParameters import PopulationParameters
from Phenotype_data_corrections import Phenotype_data_corrections