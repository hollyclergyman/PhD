#!/usr/bin/env python3

import pandas as pd
import unittest
from testcontext import PopulationParameters

class TestPopulationParameters(unittest.TestCase):
    def __init__(self):
        super(TestPopulationParameters, self).__init__(*args, **kwargs)
        self.pp = PopulationParameters()

    def test_pedigree_data(self):
        test_func = self.pp.pedigree_data(intermediate_saving=False)
        self.assertIsInstance(test_func, dict)
        self.assertIsInstance(test_func["EBV"], pd.DataFrame)
        self.assertIsInstance(test_func["Living"], pd.DataFrame)
        self.assertIsInstance(test_func["Pedigree ANG"], pd.DataFrame)
        self.assertIsInstance(test_func["Pedigree Typed ANG"], pd.DataFrame)
        self.assertIsInstance(test_func["Pedigree RDN"], pd.DataFrame)
        self.assertIsInstance(test_func["Pedigree Typed RDN"], pd.DataFrame)