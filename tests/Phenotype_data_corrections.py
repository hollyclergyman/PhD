#!/usr/bin/env python3

import pandas as pd
import numpy as np
from testcontext import Phenotype_data_corrections
from testcontext import PopulationParameters
"""
Kenneth Reitz describes this way to import dependencies from parent 
directories in the chapter about code organization in respositories.
Since this approach currently seems like the most straighforward way to
deal with such imports, it has been chosen here.
Reference:
Hitchhiker's guide to Python, page 38
"""
import unittest

class TestPhenotype_data_corrections(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestPhenotype_data_corrections, self).__init__(*args, **kwargs)
        """
        This solution fixes the point, that every __init__() function at this point
        will overwrite the unittest.TestCase() class' __init__ function. Therefore, 
        this superior __init__ function is now referred to in the upper statement.
        Solution from:
        https://stackoverflow.com/questions/17353213/init-for-unittest-testcase/17353262#17353262
        """
        self.phdc = Phenotype_data_corrections()
        self.pp = PopulationParameters()
        self.__phenotypic_data = self.pp.read_milkrecording(test=True)
        col = self.__phenotypic_data["RDN 305TL"].columns
        self.__required = np.take(col, np.random.randint(0, len(col), 2)).tolist()
        self.__fixed = np.take(col, np.random.randint(0, len(col), 2)).tolist()
        """
        The two previous statements are supposed to randomly take columns from the phenotype
        DataFrame. This procedure is supposed to avoid selective testing with only those data, 
        which were brought along during development.
        """

    def test_evaluations(self):
        test_func = self.phdc.evaluations(data=self.__phenotypic_data)
        self.assertIsInstance(test_func, dict)
        self.assertIsInstance(test_func["RDN"], dict)
        self.assertIsInstance(test_func["RDN"]["data"], pd.DataFrame)
        self.assertIsInstance(test_func["RDN"]["parameters"], pd.DataFrame)

    def test_eartag_adaption_milk_recording(self):
        test_func = self.phdc.adapt_eartags_milk_recording(
            required_columns=self.__required, 
            fixed=self.__fixed, 
            single=False, 
            test=True
        )
        self.assertIsInstance(test_func, dict)
        self.assertIsInstance(test_func["RDN"], dict)
        self.assertIsInstance(test_func["RDN"]["RDN_prior"], pd.DataFrame)
        self.assertIsInstance(test_func["RDN"]["RDN_305TL"], pd.DataFrame)
    
    def test_order_phenotypes(self):
        test_func = self.phdc.order_phenotypes(
            required_columns=self.__required,
            fixed=self.__fixed,
            single=False,
            test=True
        )
        self.assertIsInstance(test_func, dict)
        self.assertIsInstance(test_func["RDN"], dict)
        self.assertIsInstance(test_func["RDN"]["Pheno_data"], pd.DataFrame)
        self.assertIsInstance(test_func["RDN"]["Weights"], pd.DataFrame)
        
if __name__ == "__main__":
    unittest.main()