#!/usr/bin/env python3

from SheepData import SheepData
import string
import pandas as pd
import numpy as np
import random
import os
import re
import warnings
import joblib
from sklearn import linear_model
from matplotlib import pyplot as plt
from sklearn.exceptions import ConvergenceWarning


class InferPhenotypes():
    def __init__(self):
        self.shd = SheepData()
        self.sheep_folder = self.shd.sheep_folder
        
    def correlation_of_input_variables(self, selected_model:str) -> dict:
        """
        Possible options for the selected_model parameter are:
        - lasso -> runs the model based on a LASSO estimate
        - elastic -> runs the model based on an elastic net estimate
        """
        train, test = self.shd.stratified_data()
        dest_vars = [
            'Alter_Mutter_J', 
            'AblammNr', 
            'LG3_kg', 
            'TZ2_g',
            'Muskeld2_mm', 
            'Fett2_mm',  
            'Fleischigk2',
            'Bem', 
            'Ersch', 
            'Zuchtwert'
        ]
        train.index = pd.MultiIndex.from_arrays(
            [np.repeat("train", train.index.size), train.index], 
            names=["Type", "Original"]
        )
        test.index = pd.MultiIndex.from_arrays(
            [np.repeat("test", test.index.size), test.index],
            names=["Type", "Original"]
        )
        pca_data = pd.concat([train, test], axis=0)
        compress_data = self.shd.pca_compression(
            {1:pca_data}, dest_vars, on_variance_amount=True
        )
        fitted_data = compress_data[1]["data"].loc[
            :,[x for x in compress_data[1]["data"].columns if "pca_" in x]
        ]
        pca_data = pd.concat([pca_data, fitted_data], axis=1)
        x_vars = [
            'Gebmonat', 
            'Gebtyp', 
            'Rasse', 
            'Alter1_Tage',
            'Alter2_Tage',
            'Alter3_Tage',
            'LG1_kg',
            'Muskeld1_mm',
            'Fettd1_mm', 
            'Fleischigk1'
        ]
        _train = pca_data.loc[pd.IndexSlice["train",:],:]
        _test = pca_data.loc[pd.IndexSlice["test",:],:]
        _train.reset_index(drop=False, inplace=True)
        _train.index = _train["Original"]
        _train.drop(["Type", "Original"], axis=1, inplace=True)
        _test.reset_index(drop=False, inplace=True)
        _test.index = _test["Original"]
        _test.drop(["Type", "Original"], axis=1, inplace=True)
        predictions = pd.DataFrame()
        predictors = {}
        train_x = _train.loc[:,x_vars].subtract(_train.loc[:,x_vars].mean())/_train.loc[:,x_vars].std()
        #train_x = _train.loc[:,x_vars]
        pattern = re.compile(r"pca\_[0-9]{1,}")
        compressed_columns = list(filter(pattern.match, _train.columns))
        for var in compressed_columns:
            train_y = _train.loc[:,var]
            with warnings.catch_warnings():
                warnings.filterwarnings("ignore")
                if "lasso" in selected_model:
                    cv_model = linear_model.LassoLarsCV(
                        cv=np.int(np.ceil(len(x_vars)/2))
                    )
                elif "elastic" in selected_model:
                    cv_model = linear_model.ElasticNetCV(
                        cv=np.int(np.ceil(len(x_vars)/2))
                    )
                else:
                    print("no valid choice")
                    continue
                cv_fit = cv_model.fit(train_x, train_y)
                _alpha = cv_fit.alpha_
                if "lasso" in selected_model:
                    model = linear_model.LassoLars(
                        alpha=_alpha, max_iter=2*len(train_x.index)
                    )
                elif "elastic" in selected_model:
                    model = linear_model.ElasticNet(
                        alpha=_alpha, max_iter=2*len(train_x.index)
                    )
                else:
                    continue
            fit = model.fit(train_x, train_y)
            predicted = pd.Series(
                name=var, 
                index=test.index, 
                data=fit.predict(test.loc[:,x_vars])
            )
            predictions = pd.concat(
                [predictions, predicted], 
                axis=1, 
                sort=False
            )
            predictors[var] = fit
        results = self.shd.reverse_pca(
            pca_data=predictions,
            components=compress_data[1]["PC Scores"],
            _mean=train.loc[:,dest_vars].mean(axis=0),
            _columns=np.array(dest_vars)
        )
        results.index = pd.MultiIndex.from_tuples(
            results.index, names=["Type", "Original"]
        )
        predictions.index = pd.MultiIndex.from_tuples(
            predictions.index, names=["Type", "Original"]
        )
        return {"Predictions": predictions, 
                "True values compressed": _test.loc[:,compressed_columns],
                "True values uncompressed": test.loc[:,dest_vars],
                "Results": results, 
                "Predictors": predictors, 
                "Mean": train.loc[:,dest_vars].mean(axis=0),
                "Components": compress_data[1]["PC Scores"]
               } 
    
    def random_x_values(
        self, 
        start_value: int,
        ear_tag_cols:tuple, 
        age_variables:tuple,
        normal_variables:tuple,
        binomial_variables:tuple,
        data:pd.DataFrame,
        diff1:int,
        diff2:int) -> pd.DataFrame:
        r = pd.DataFrame(
            columns=ear_tag_cols + age_variables + normal_variables + binomial_variables,
            index=np.arange(start_value, start_value + 2)
        )
        for column in r.columns:
            if column in ear_tag_cols:
                r.loc[:,column] = self.create_random_eartags(data[column], 2)
            elif column in age_variables:
                continue
            elif column in binomial_variables:
                r.loc[:,column] = np.random.poisson(
                    data[column].quantile(0.4),
                    2
                )
            elif column in normal_variables:
                r.loc[:,column] = np.random.normal(
                    data[column].mean(),
                    data[column].std(),
                    2
                )
        r.loc[:,"Alter2_Tage"] = r.loc[:,"Alter1_Tage"] + diff1
        r.loc[:,"Alter3_Tage"] = r.loc[:,"Alter2_Tage"] + diff2
        return r

    def create_random_values(self, selected_model:str, length_of_new=40) -> pd.DataFrame:
        """
        The options for the selected_model parameter are: 
        - "lasso"
        - "elastic"
        """
        if length_of_new % 2 != 0:
            print("Even number required")
            return None
        original = self.shd.data_cleaning()
        predict = self.correlation_of_input_variables(
            selected_model=selected_model
        )
        normal_variables = (
            'LG1_kg',
            "Rasse",
            'Muskeld1_mm',
            'Fettd1_mm', 
            'Fleischigk1'
        )
        binomial_variables = (
            "Gebmonat",
            "Gebtyp",
            "Alter1_Tage"
        )
        predicted_variables = ( 
            'Alter_Mutter_J', 
            'AblammNr', 
            'LG3_kg', 
            'TZ2_g',
            'Muskeld2_mm', 
            'Fett2_mm',  
            'Fleischigk2',
            'Bem', 
            'Ersch', 
            'Zuchtwert'
        )
        ear_tag_cols = ("Nummer", "Vater", "Mutter")
        age_variables = ("Alter2_Tage", "Alter3_Tage")
        diff1 = np.unique(original["Alter2_Tage"] - original["Alter1_Tage"])[0]
        diff2 = np.unique(original["Alter3_Tage"] - original["Alter2_Tage"])[0]
        prediction_x = [
            'Gebmonat', 
            'Gebtyp', 
            'Rasse', 
            'Alter1_Tage',
            'Alter2_Tage',
            'Alter3_Tage',
            'LG1_kg',
            'Muskeld1_mm',
            'Fettd1_mm', 
            'Fleischigk1'
        ]
        """
        The prediction_x list of variables is a bunch of variables which is autogenerated 
        based on random distributions. These variables are essentially generated in the 
        random_x_values() function.
        """
        results = pd.DataFrame()
        for i in range(1, length_of_new):
            if i % 2 == 1:
                while True:
                    x_vars = self.random_x_values(
                        start_value=i,
                        ear_tag_cols=ear_tag_cols,
                        age_variables=age_variables,
                        normal_variables=normal_variables,
                        binomial_variables=binomial_variables,
                        data=original,
                        diff1=diff1,
                        diff2=diff2
                    )
                    pca_compressed = pd.DataFrame(
                        columns=list(predict["Predictors"].keys())
                    )
                    normalized = x_vars.loc[:,prediction_x].subtract(
                        x_vars.loc[:,prediction_x].mean())/x_vars.loc[:,prediction_x].std()
                    for _column in predict["Predictors"].keys():
                        _predictor = predict["Predictors"][_column]
                        pca_compressed[_column] = _predictor.predict(normalized.fillna(0).values)
                    predicted = self.shd.reverse_pca(
                        pca_data=pca_compressed,
                        components=predict["Components"],
                        _mean=predict["Mean"],
                        _columns=predicted_variables
                    )
                    predicted.index = x_vars.index
                    x_vars = pd.concat([x_vars, predicted], axis=1)
                    x_vars.dropna(how="all", axis=0, inplace=True)
                    quality_controlled = self.predicted_quality_control(
                        data=x_vars, 
                        original=original,
                        check_columns=age_variables + normal_variables + binomial_variables
                    )
                    if quality_controlled is None:
                        continue
                    elif quality_controlled.index.size < 2:
                        continue
                    else:
                        results = pd.concat([results, quality_controlled], axis=0)
                        break
            else:
                continue
        return results

    def predicted_quality_control(self, data:pd.DataFrame, original:pd.DataFrame, check_columns:tuple):
        data = data.astype(np.float64)
        for index in data.index:
            r = []
            for column in check_columns:
                if abs(original.loc[:,column].mean() - data.loc[index,column]) <= original[column].std():
                    r.append(1)
                else:
                    r.append(0)
            if sum(r) < len(check_columns)/2:
                data.drop(index, inplace=True, axis=0)
        if data.index.size > 0:    
            data["Alter_Mutter_J"] = np.ceil(data["Alter_Mutter_J"])
            data["AblammNr"] = np.floor(data["AblammNr"])
            data.loc[data["Zuchtwert"] > original["Zuchtwert"].mean() + original["Zuchtwert"].std(), "Zuchtwert"] = 0
            data.loc[data["Zuchtwert"] < original["Zuchtwert"].mean() - original["Zuchtwert"].std(), "Zuchtwert"] = 0
            data["Rasse"] = data["Rasse"].round(0)
            data.loc[(data["Rasse"] > 4) | (data["Rasse"] < 1), "Rasse"] = np.random.randint(
                1, 4, data.loc[(data["Rasse"] > 4) | (data["Rasse"] < 1), "Rasse"].index.size
            )
            data.loc[(data["Gebmonat"] == 0) & (data["Alter1_Tage"] < data["Alter1_Tage"].quantile(0.4)), "Gebmonat"] = 2
            data.loc[(data["Gebmonat"] == 0) & (data["Alter1_Tage"] >= data["Alter1_Tage"].quantile(0.4)), "Gebmonat"] = 1
            data.loc[(data["Gebtyp"] == 0), "Gebtyp"] = np.random.randint(
                1, 3, data.loc[(data["Gebtyp"] == 0), "Gebtyp"].index.size
            )
            return data
        else:
            return None

    def create_random_eartags(self, values, required_length) -> np.int64:
        l = []
        for nummer in np.random.choice(values, size=required_length):
            country = str(nummer)[:3]
            l.append(
                country + "".join(random.SystemRandom().choice(string.digits) for _ in range(14))
            )
            # The solution to this problem of creating numeric strings of designated lengths has 
            # been discussed extensively on stackoverflow. The solution originates from:
            # https://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits/23728630#23728630
        return np.int64(l)
    
    def infer_data(
        self, 
        selected_model:str, 
        corrected:bool, 
        length_of_new=10, 
        sort_by_breed=False):
        x_variables = self.create_random_values(
            selected_model=selected_model, 
            length_of_new=length_of_new
        )
        if corrected is True and sort_by_breed is True:
            trained_model_folders = [
                x for x in os.listdir(
                    os.path.join(self.sheep_folder, "model", "corrected", "sorted")
                ) if os.path.isdir(os.path.join(self.sheep_folder, "model", "corrected", "sorted", x))
            ]
            path = os.path.join(self.sheep_folder, "model", "corrected", "sorted")
        elif corrected is True and sort_by_breed is False:
            trained_model_folders = [
                x for x in os.listdir(
                    os.path.join(self.sheep_folder, "model", "corrected", "unsorted")
                ) if os.path.isdir(os.path.join(self.sheep_folder, "model", "corrected", "unsorted", x))
            ]
            path = os.path.join(self.sheep_folder, "model", "corrected", "unsorted")
        elif corrected is False and sort_by_breed is True:
            trained_model_folders = [
                x for x in os.listdir(
                    os.path.join(self.sheep_folder, "model", "uncorrected", "sorted")
                ) if os.path.isdir(os.path.join(self.sheep_folder, "model", "uncorrected", "sorted", x))
            ]
            path = os.path.join(self.sheep_folder, "model", "uncorrected", "sorted")
        else:
            trained_model_folders = [
                x for x in os.listdir(
                    os.path.join(self.sheep_folder, "model", "uncorrected", "unsorted")
                ) if os.path.isdir(os.path.join(self.sheep_folder, "model", "uncorrected", "unsorted", x))
            ]
            path = os.path.join(self.sheep_folder, "model", "uncorrected", "unsorted")
        if "fattening" in trained_model_folders:
            if sort_by_breed is True:
                sorting = "sorted"
            else:
                sorting = "unsorted"
            fattening_results = self.__predict_phenotypes(
                selected_model=selected_model,
                corrected=corrected,
                sorting=sorting, 
                directories=path,
                dataset="fattening",
                x_variables=x_variables)
        if "slaughter" in trained_model_folders:
            try:
                slaughter_results = self.__predict_phenotypes(
                    selected_model=selected_model,
                    corrected=corrected,
                    sorting=sorting,
                    dataset="slaughter",
                    directories=path,
                    x_variables=fattening_results
                )
                slaughter_results.drop(
                    [col for col in slaughter_results.columns if col in fattening_results.columns], 
                    axis=1, 
                    inplace=True
                )
                res = pd.concat([fattening_results, slaughter_results], axis=1)
                return res
            except NameError:
                if sort_by_breed is True:
                    sorting = "sorted"
                else:
                    sorting = "unsorted"
                slaughter_results = self.__predict_phenotypes(
                    selected_model=selected_model,
                    corrected=corrected,
                    sorting=sorting,
                    dataset="slaughter",
                    directories=path,
                    x_variables=x_variables
                )
                return slaughter_results
    
    def __predict_phenotypes(
        self, 
        selected_model:str, 
        corrected:bool,
        sorting:str, 
        dataset:str, 
        directories:str,
        x_variables:pd.DataFrame) -> pd.DataFrame:
        _path = directories.split("/")
        _path.append(dataset)
        path = "/".join(_path)
        model_pattern = re.compile(r"%s\_[a-zA-Z0-9\_]+.joblib"%re.escape(selected_model))
        pca_pattern = re.compile(r"pca\_[0-9]+")
        models = sorted([
            x for x in os.listdir(path) if model_pattern.match(x)
        ])
        _df = pd.DataFrame(index=x_variables.index)
        if "slaughter" in dataset:
            x_vars = x_variables.loc[
                :,[x for x in x_variables.columns if x not in self.shd.slaughter_vars]
            ]
        if "fattening" in dataset:
            x_vars = x_variables.loc[
                :,[x for x in x_variables.columns if x not in self.shd.raising_vars]
            ]
        x_vars_centered = x_vars.subtract(x_vars.mean())/x_vars.std()
        for model in models:
            _model = joblib.load(
                os.path.join(self.sheep_folder, path, model)
            )
            name = "_".join(model.split(".")[0].split("_")[1:])
            if not pca_pattern.match(name):
                if "slaughter" in dataset and name in self.shd.slaughter_vars:
                    _df[name] = _model.predict(x_vars_centered.fillna(0))
                elif "fattening" in dataset and name in self.shd.raising_vars:
                    _df[name] = _model.predict(x_vars_centered.fillna(0))
                else:
                    continue
            else:
                _df[name] = _model.predict(x_vars_centered.fillna(0))
        reversed = self.__reverse_pca_to_original(
            data=_df[[col for col in _df.columns if pca_pattern.match(col)]],
            sorting=sorting,
            path=path
        )
        res = pd.concat([x_vars, reversed], axis=1)
        if len([col for col in _df.columns if not pca_pattern.match(col)]) > 0:
            for _col in [col for col in _df.columns if not pca_pattern.match(col)]:
                res = pd.concat([res, _df[_col]], axis=1)
        return res

    def __reverse_pca_to_original(
        self, 
        data:pd.DataFrame,
        path:str,
        sorting:str) -> pd.DataFrame:
        _eigenvalues = pd.read_csv(
            os.path.join(path, "Eigenvalues.csv"), 
            index_col=0,
            header=0
        )
        _mean = pd.read_csv(
            os.path.join(path, "Mean.csv"), 
            index_col=0,
            header=0
        )
        pc_scores = pd.read_csv(
            os.path.join(path, "PCScores.csv"), 
            index_col=[0,1],
            header=0
        )
        res = self.shd.reverse_pca(
            pca_data=data,
            components=pc_scores,
            _mean=_mean,
            _columns=_mean.index
        )
        return res