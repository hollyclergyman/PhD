#!/usr/bin/env python3

import pandas as pd
import numpy as np
from scipy import stats
import statsmodels.api as sm
from statsmodels.formula.api import ols
from sklearn import linear_model
import re
import os
import gc
from functools import partial
import itertools
import multiprocessing as mp
from PopulationParameters import PopulationParameters
from Pedigree import Pedigree

class TestCorrelation():
    def __init__(self):
        self.__pp = PopulationParameters()
        self.__ped = Pedigree()

    def read_data(self, required, merged=False, read=False):
        r = {}
        if merged is True:
            pattern_merged = re.compile(r"Merged\ [A-Z]+.csv")
            if len(list(filter(pattern_merged.match, self.__pp.milkrecording_csv))) > 0 and read is True:
                for _file in list(filter(pattern_merged.match, self.__pp.milkrecording_csv)):
                    fname = _file.split()[1].split(".")[0]
                    r[fname] = pd.read_csv(
                        os.path.join(self.__pp.milkrecording_folder, _file), 
                        index_col=0, 
                        header=0
                    )
                return r
            pedigree = self.__pp.pedigree_data()
        _in = self.__pp.read_milkrecording()
        pattern = re.compile(r"[A-Z]{1,}\ 305TL")
        matching = list(filter(pattern.match, _in.keys()))
        for breed in matching:
            _d = _in[breed]
            _data = pd.DataFrame()
            bname = breed.split(" ")[0]
            for parameter in required:
                try:
                    _p = _d.loc[:,parameter]
                    _data = pd.concat([_data, _p], axis=1, sort=False)
                except KeyError:
                    continue
            if len(_data.columns) < 1:
                continue
            elif merged is True:
                self.__pheno = _data
                pattern_ped = re.compile(r"Pedigree\ [A-Za-z\ ]*%s"%re.escape(bname))
                ped_keys = list(filter(pattern_ped.match, pedigree.keys()))
                ped_df = pd.DataFrame()
                for table in ped_keys:
                    _table = pedigree[table]
                    _table.columns = [x.lower() for x in _table.columns]
                    ped_df = pd.concat([ped_df, _table], axis=0)
                ped_df.reset_index(inplace=True, drop=False)
                ped_df = ped_df.loc[:,["Ear tag", "ear tag sire", "ear tag dam", "year of birth"]]
                ped_df.columns = ["Ear tag", "Sire", "Dam", "Year of Birth"]
                ped_df = self.__ped.make_animals_unique(ped_df)
                divided = self.__pp.divide_df(ped_df)
                _r = []
                enc = {
                    "dt": _in['305TL datetime'],
                    "float": ['MILCH_KG', 'FETT_KG', 'EIWEISS_KG']
                }
                with mp.Pool(processes=mp.cpu_count()) as pool:
                    to_execute = partial(
                        self.merge_phenotype_and_pedigree, 
                        enc
                    )
                    _res = [v for v in pool.map(to_execute, divided) if v is not None]
                    if len(_res) > 0:
                        res = pd.concat(_res, axis=0)
                    del _res
                    gc.collect()
                if 'res' in locals():
                    r[bname] = res
                    res.to_csv(
                        os.path.join(self.__pp.milkrecording_folder, f"Merged {bname}.csv")
                    )
                    del res
                    gc.collect()
                else:
                    continue
            else:
                r[bname] = _data
        return r
    
    def merge_phenotype_and_pedigree(self, enc, pedigree):
        """
        This function works along the pedigree index and tries to align this 
        index with repsective values for the phenotype. In case of missing phenotype
        values, these are set to 0.
        """
        r = pd.DataFrame()
        cols = pedigree.columns.tolist() + self.__pheno.columns.tolist()
        for trait in cols:
            if trait in enc['dt']:
                _dtype = 'datetime64[ns]'
            elif trait in enc['float']:
                _dtype = np.float64
            else:
                _dtype = np.int64
            _r = pd.Series(index=np.unique(pedigree.index), name=trait, dtype=_dtype)
            for animal in np.unique(pedigree.index):
                if trait in pedigree.columns:
                    try:
                        _r.loc[animal] = pedigree.loc[animal, trait]
                    except KeyError:
                        _r.loc[animal] = 0
                else:
                    try:
                        data = self.__pheno.loc[animal, trait]
                        if isinstance(data, pd.Series):
                            """
                            The reason for having this condition is that animals can have 
                            multiple records for different lactations, etc..
                            Therefore, it is necessary to ensure that repeated measurements
                            are evaluated correctly, which is done by appending the values to 
                            the resulting series.
                            """
                            _r.append(data, ignore_index=False)
                        else:
                            _r.loc[animal] = data
                    except KeyError:
                        _r.loc[animal] = 0
            r = pd.concat([r, _r], axis=1)
            del _r
            gc.collect()
        for _animal in np.unique(r[r.duplicated()]):
            for column in pedigree.columns:
                try:
                    value = np.unique(pedigree.loc[_animal, column])
                    r.loc[animal, column] = value
                    """
                    In case of repeated measurements, the pedigree records might be missing. To 
                    correct on that, these records are copied in this loop.
                    """
                except KeyError:
                    break
                    # The break statement jumps to the loop running above this loop.
                    # This is not the best solution, but it works suitably fine.
        return r
    
    def test_distribution(self, required, categorical, data=None, save=False):
        if data is None:
            data = self.read_data(required)
        res = pd.DataFrame()
        for breed, values in data.items():
            _res = pd.DataFrame(
                columns=values.columns,
                index=["normal", "binomial"]
            )
            for trait in values.columns:
                if categorical is not None and trait in categorical:
                    continue
                else:
                    for l in _res.index:
                        if "normal" in l:
                            _stats = stats.kurtosis(
                                values.loc[:,trait],
                                axis=None,
                                nan_policy="omit"
                            )
                            """
                            Since the normaltest provided in the scipy.stats() class does not provide
                            satisfactory precision when dealing with natural data, only the kurtosis, which
                            is also part of the normaltest properties, is calculated. Values smaller than 
                            0.2 can be considered to originate from a normal distribution:
                            https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.kurtosis.html
                            The paper related to this test is:
                            Distribution of the Kurtosis Statistic b_2 for Normal Samples from F.J. Anscombe and J. Glynn
                            """
                            _res.loc[l, trait] = _stats
                        if "binomial" in l:
                            _l = []
                            """
                            Testing for binomial distributions is especially important when dealing with 
                            categorical data, such as amount of lactations per cow, but it has been adapted to 
                            check the matching of value occurence in datasets in the whole context. Therefore, the 
                            value returns a rather quantitative approach. As a mean of evaluation, the mean of all returned
                            p-values is taken into consideration
                            """
                            for v in np.unique(values.loc[:,trait]):
                                _l.append(
                                    stats.binom_test(len(values[values.loc[:,trait] == v]), len(values.loc[:,trait]), p=0.05)
                                )
                            _res.loc[l, trait] = np.mean(_l)
            _res.index = pd.MultiIndex.from_arrays(
                [
                    list(itertools.repeat(breed, len(_res.index))), 
                    _res.index
                ],
                names=["Breed", "Distributions"]
            )
            res = pd.concat([res, _res], axis=0, sort=False)
        if save is True:
            res.to_csv(f"{self.__pp.milkrecording_folder}/Distribution_{'_'.join(required)}.csv")
        return res

    def test_correlation(self, required, categorical):
        data = self.read_data(required)
        distribution = self.test_distribution(required, categorical, data=data, save=False)
        correlation_values = pd.DataFrame()
        for breed in data.keys():
            breed_distribution = distribution.loc[pd.IndexSlice[breed,:],:]
            breed_data = data[breed]
            correlated = pd.DataFrame(
                index=breed_distribution.columns, 
                columns=breed_distribution.columns
            )
            for measure in breed_distribution.columns:
                for _measure in breed_distribution.columns:
                    if measure and _measure in categorical:
                        continue
                    if measure not in categorical and _measure not in categorical:
                        """
                        The incentive behind this condition is that correlation can not be calculated between categorical values or 
                        between categorical and continuous values
                        """
                        _corr = breed_data.loc[:,[measure, _measure]].corr(method="pearson")
                        """
                        Correlation determination according to Pearson does not take the distribution into account, as 
                        relatedness is not necessarily related to the densitiy function, the values follow
                        https://stats.stackexchange.com/questions/3730/pearsons-or-spearmans-correlation-with-non-normal-data/3733#3733
                        """
                        try:
                            correlated.loc[measure, _measure] = _corr.loc[measure, _measure]
                        except ValueError:
                            if measure in _measure:
                                correlated.loc[measure, _measure] = 1
                            else:
                                continue
                        print(correlated)
                    elif measure in categorical or _measure in categorical:
                        if measure in categorical:
                            _cat_var = measure
                            value = _measure
                            _distribution = self.determine_distribution(
                                breed_distribution, 
                                breed, 
                                _measure
                            )
                            if _distribution is None:
                                print(measure, _measure, breed_distribution, sep="\t")
                        elif _measure in categorical:
                            _cat_var = _measure
                            value = measure
                            _distribution = self.determine_distribution(
                                breed_distribution, 
                                breed, 
                                measure
                            )
                            if _distribution is None:
                                print(measure, _measure, breed_distribution, sep="\t")
                        print(_distribution)
                        if "normal" in _distribution:
                            """
                            Since a correlation between categorical variables and continuous 
                            variables is hard to prove, the easiest way is to make up an ANOVA
                            analysis, which tests, whether the categorical variable makes up for 
                            a significant difference. In case of a non-normal distribution, 
                            Kruskal-Wallis is used.
                            """
                            _idx = required.index(value)
                            if (len(required) - _idx) >= 0:
                                print(required, len(required) - _idx, sep="\t")
                                if (len(required) - _idx) > 0:
                                    ival = (len(required) - _idx) - 1
                                else:
                                    ival = 0
                                if required[ival] not in categorical:
                                    other_value = required[ival]
                                    print(other_value)
                            elif (_idx + 1) <= len(required):
                                print(required, _idx + 1, sep="\t")
                                if required[_idx + 1] not in categorical:
                                    other_value = required[_idx + 1]
                            elif (_idx - 1) <= len(required):
                                print(required, _idx - 1, sep="\t")
                                if required[_idx - 1] not in categorical:
                                    other_value = required[_idx + 1]
                            else:
                                return None
                            """
                            To avoid the ANOVA analysis from running into low confidence interval errors,
                            another variable has been added to the model, without taking these results into
                            further account. 
                            """
                            _data = breed_data.loc[:,[_cat_var, value, other_value]].dropna(how="all", axis=1)
                            _data.replace(np.NaN, 0, inplace=True)
                            _data.sort_values(_cat_var, axis=0, inplace=True)
                            _model = f"{_cat_var} ~ {value} * {other_value}"
                            model = ols(_model, _data)
                            lm = model.fit()
                            table = sm.stats.anova_lm(lm, type=2).F
                            # Directly drawn from the examples on the statsmodels webpage:
                            # https://www.statsmodels.org/stable/anova.html?highlight=anova
                            #corr.loc[measure, _measure] = lm.params.loc[value]
                            # If the regression factors are of interest, these can be accessed 
                            # using the params property on the lm variable. This step returns a 
                            # pd.Series containing regression factors for all variables included 
                            # in the model.
                            print(table)
                            correlated.loc[measure, _measure] = table.loc[value]
                        if "binomial" in _distribution:
                            _data = breed_data.loc[:,[_cat_var, value]].dropna(how="all", axis=1)
                            _data.replace(np.NaN, 0, inplace=True)
                            _data.sort_values(_cat_var, axis=0, inplace=True)
                            _mean = pd.Series(index=np.unique(_data.loc[:,_cat_var]))

                            for fixed in np.unique(_data.loc[:,_cat_var]):
                                try:
                                    _v = _data.loc[_data[_cat_var] == fixed]
                                except KeyError:
                                    print(_cat_var, fixed)
                                    continue
                                _mean.loc[fixed] = _v[value].mean()
                            l = []
                            for _fixed in np.unique(_data.loc[:,_cat_var]):
                                _v = _data.loc[_data[_cat_var] == fixed]
                                _d = _v[value]
                                statistics, p = stats.kruskal(
                                    _d,
                                    _mean.values, 
                                    nan_policy="omit"
                                )
                                # The intention behind this test is to figure out, whether a sample under 
                                # certain fixed conditions deviates from a mean of all fixed conditions
                                l.append(p)
                            print(_cat_var, value, np.quantile(l, 0.25), np.median(l), np.quantile(l, 0.75), sep="\t")
                            correlated.loc[measure, _measure] = np.median(l)
            correlated.index = pd.MultiIndex.from_arrays(
                [list(itertools.repeat(breed, len(correlated.index)))],
                names=["Breed", "Correlation"]
            )
            correlation_values = pd.concat([correlation_values, correlated], axis=0, sort=False)
        return correlation_values

    def determine_distribution(self, breed_distribution, breed, measure):
        if breed_distribution.loc[pd.IndexSlice[breed, "normal"], measure] < 0.2:
            """
            The threshold used in this condition to tell, whether the sample might be derived 
            has been explained in detail previously. It is based, as already declared, on the 
            paper Distribution of the Kurtosis Statistic b_2 for Normal Samples
            """
            return "normal"
        elif breed_distribution.loc[pd.IndexSlice[breed,"normal"], measure] > 0.2 and breed_distribution.loc[pd.IndexSlice[breed,"binomial"], measure] > 0:
            """
            The overall assumption is that all data which is not normally distributed follows a binomial distribution
            """
            return "binomial"
        
    def linear_model(self, fixed, randomized):
        data = self.read_data(fixed + randomized, merged=True, read=True)
        for key, _data in data.items():
            print(_data)
            for trait in fixed:
                for _trait in value.columns:
                    if _trait == trait:
                        continue
                    else:
                        model = linear_model.LinearRegression()
                        #other_traits = [v for v in fixed if v not in trait] + randomized
                        try:
                            fit_ = model.fit(_data[trait].to_numpy().reshape(-1,1), _data.loc[:,_trait].to_numpy())
                        except TypeError:
                            continue
                        print(fit_.coef_)
        
