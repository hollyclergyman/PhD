#!/usr/bin/env python3

import argparse
from PopulationParameters import PopulationParameters
from Phenotype_data_corrections import Phenotype_data_corrections
from Lactation_modelling import Lactation_modelling
from LactationCurves import LactationCurves
from TestCorrelation import TestCorrelation
#from Heritability import Heritability
from Pedigree import Pedigree

class Calls():
    def __init__(self):
        self.pp = PopulationParameters()
        self.phdc = Phenotype_data_corrections()
        self.lact = Lactation_modelling()
        #self.herit = Heritability()
        self.testcr = TestCorrelation()
        self.ped = Pedigree()

    def arguments(self):
        parser = argparse.ArgumentParser(
            description="Processing pedigree and phenotypic information"
        )

        parser.add_argument("-ph", "--phenotype", action="store_const", const=True)
        parser.add_argument("-pp", "--population", action="store_const", const=True)
        parser.add_argument("-her", "--heritability", action="store_const", const=True)
        parser.add_argument("-lact", "--lactation", action="store_const", const=True)
        parser.add_argument("-ev", "--evaluation", action="store_const", const=True)
        parser.add_argument("-ped", "--pedigree", action="store_const", const=True)
        """
        nargs="?" is required to make the argument optional. 
        Source: 
        https://stackoverflow.com/questions/15754208/how-to-make-argument-optional-in-python-argparse/15754361#15754361
        """
        parser.add_argument("-adt", "--adaptmilkrecording", action='store_const', const=True)
        parser.add_argument("-s", "--single", action='store_const', const=True)
        parser.add_argument("-t", "--test", action="store_const", const=True)
        parser.add_argument("-req", "--required", type=str, nargs="+")
        parser.add_argument("-fix", "--fixed", type=str, nargs="+")
        parser.add_argument("-rnd", "--random", type=str, nargs="+")
        parser.add_argument("-wr", "--write", action="store_const", const=True)
        parser.add_argument("-raw", "--raw", action="store_const", const=True)
        parser.add_argument("-et", "--eartag", action="store_const", const=True)
        parser.add_argument("-ys", "--year-season", action="store_const", const=True)
        parser.add_argument("-par", "--parents", action="store_const", const=True)

        parser.add_argument("-pd", "--pedigreedata", action='store_const', const=True)
        parser.add_argument("-pmr", "--processmilkrecording",action='store_const', const=True)
        parser.add_argument("-pme", "--pmergebnisse",action='store_const', const=True)
        
        parser.add_argument("-ma", "--mixtureanalysis", action='store_const', const=True)
        parser.add_argument("-br", "--breed", type=str, nargs="?")
        parser.add_argument("-thrs", "--threshold", type=str, nargs="?")

        parser.add_argument("-recalc", "--recalculate", action="store_const", const=True)
        parser.add_argument("-lm", "--lactmodel", action='store_const', const=True)
        parser.add_argument("-clm", "--comparelactmodel", action="store_const", const=True)
        
        parser.add_argument("-tcrl", "--testcorrelation", action="store_const", const=True)
        parser.add_argument("-cat", "--categoricals", type=str, nargs="+")
        parser.add_argument("-tdst", "--testdistribution", action="store_const", const=True)
        parser.add_argument("-corr", "--correlation", action="store_const", const=True)
        parser.add_argument("-lin", "--linearmodel", action="store_const", const=True)
        parser.add_argument("-read", "--read", action="store_const", const=True)

        parser.add_argument("-dmu", "--dmu", action="store_const", const=True)
        parser.add_argument("-bf90", "--blupf90", action="store_const", const=True)
        parser.add_argument("-typ", "--typed", action="store_const", const=True)
        parser.add_argument("-sm", "--save-memory", action="store_const", const=True)
        parser.add_argument("-305tl", "--305TagesLeistung", action="store_const", const=True)
        parser.add_argument("-pmonly", "--PMergebnisse", action="store_const", const=True)
        parser.add_argument("-td", "--testday", type=str, nargs="+")
        """
        Working with action='store_const' and const=True seems to be the only way to determine, what 
        will be the output of an option, which is set to True if set. The solution is documented in 
        the Python3 documentation without highlighting its usefulness.
        https://docs.python.org/3/library/argparse.html#action
        The function returns the values from the command line interface as a dictionary. Solution has been suggested
        in:
        https://stackoverflow.com/questions/16878315/what-is-the-right-way-to-treat-python-argparse-namespace-as-a-dictionary/16878364#16878364
        """
        return vars(parser.parse_args())
        
    def call_function(self):
        _in = self.arguments()
        r = {k:v for k,v in _in.items() if v is not None}
        """
        This step removes all empty fields from the dict, returned from 
        argparse.
        https://stackoverflow.com/questions/2544710/how-i-can-get-rid-of-none-values-in-dictionary/2544761#2544761
        """
        for key in r.keys():
            if "phenotype" in key and r[key] is True:
                self.call_phenotypes(r)
                return None
            if "population" in key and r[key] is True:
                self.call_population(r)
                return None
            if "heritability" in key and r[key] is True:
                self.call_heritability(r)
                return None
            if "lactation" in key and r[key] is True:
                self.call_lactations(r)
                return None
            if "testcorrelation" in key and r[key] is True:
                self.call_testcorrelation(r)
                return None
            if "pedigree" in key and r[key] is True:
                self.call_ped_adaption(r)
                return None
    
    def call_lactations(self, r):
        if len(r["required"]) > 0:
            required = r["required"]
        else:
            print("Required columns must be specified, none provided")
            return None
        if "recalculate" in r.keys():
            recalc = True
        elif "recalculate" not in r.keys():
            recalc = False
        if "write" in r.keys():
            save = True
        elif "write" not in r.keys():
            save = False
        if "read" in r.keys():
            read = True
        elif "read" not in r.keys():
            read = False
        if "lactmodel" in r.keys() and r["lactmodel"] is True:
            self.lact.lact_model(trait=required, save=save)
            return None
        elif "comparelactmodel" in r.keys() and r["comparelactmodel"] is True:
            self.lact.compare_curve_to_yield(
                trait=required, 
                recalculate=recalc, 
                write=save, 
                read=read
            )
            return None

    def call_testcorrelation(self, r):
        if "testdistribution" in r.keys() or "correlation" in r.keys():
            if len(r["required"]) > 0:
                required = r["required"]
            else:
                print("Required columns must be specified, none provided")
                return None
            if len(r["categoricals"]) > 0:
                categorical = r["categoricals"]
            else:
                categorical = None
        if "testdistribution" in r.keys():
            self.testcr.test_distribution(
                required, 
                categorical
            )
            return None
        if "correlation" in r.keys():
            self.testcr.test_correlation(
                required, 
                categorical
            )
            return None
        if "linearmodel" in r.keys():
            if len(r["fixed"]) > 0:
                fixed = r["fixed"]
            else:
                print("Fixed values are required")
                return None
            if len(r["random"]) > 0:
                random = r["random"]
            else:
                print("Random values are required")
                return None
            self.testcr.linear_model(
                fixed,
                random
            )

    def call_phenotypes(self, r):
        if "evaluation" in r.keys():
            self.phdc.evaluations()
            return None
        try:
            t = r["test"]
        except KeyError:
            t = False
        try:
            s = r["single"]
        except KeyError:
            s = False
        try:
            r["write"]
            w = True
        except KeyError:
            w = False
        try:
            r["eartag"]
            et = True
        except KeyError:
            et = False
        try:
            r["year_season"]
            year_season = True
        except KeyError:
            year_season = False
        try:
            r["save_memory"]
            save_memory = True
        except KeyError:
            save_memory = False
        try:
            r["305TagesLeistung"]
            tl305 = True
        except KeyError:
            tl305 = False
        try:
            r["PMergebnisse"]
            pmonly = True
        except KeyError:
            pmonly = False
        try:
            r["random"]
            random_effects = r["random"]
        except KeyError:
            random_effects = []
        if len(r["required"]) > 0:
            required = r["required"]
        else:
            print("Required columns must be specified, none provided")
            return None
        if len(r["fixed"]) > 0:
            fixed = r["fixed"]
        else:
            print("Fixed columns required, none provided")
            return None
        try:
            r["lactmodel"]
            lact_model = True
        except KeyError:
            lact_model = False
        try:
            r["testday"]
            testday_traits = r["testday"]
        except KeyError:
            testday_traits = None
        if "adaptmilkrecording" in r.keys():
            self.phdc.adapt_milk_recording(
                required_columns=required,
                fixed=fixed,
                single=s,
                test=t, 
                write=w,
                correct_eartags=et,
                year_season=year_season,
                save_memory=save_memory,
                tl305=tl305,
                pmonly=pmonly,
                random_effects=random_effects,
                lact_model=lact_model,
                testday=testday_traits
            )
            return None

    def call_population(self, r):
        if "pedigreedata" in r.keys():
            self.pp.pedigree_data()
            return None
        if "processmilkrecording" in r.keys():
            self.pp.read_milkrecording()
            return None
        if "mixtureanalysis" in r.keys():
            try:
                self.pp.mixture_analysis(breed=r["breed"], thrsh=r["threshold"])
                return None
            except IndexError:
                print("Breed and Threshold required")

    def call_ped_adaption(self, r):
        if "write" in r.keys():
            saving = True
        else:
            saving = False
        if "typed" in r.keys():
            typed = True
        else:
            typed = False
        if "read" in r.keys():
            read = True
        else:
            read = False
        if "eartag" in r.keys():
            self.ped.pedigree_adaption(saving=saving)
            return None
        if "blupf90" in r.keys():
            self.ped.formatForBlupF90(
                read=read, 
                save=saving, 
                include_typed=typed
            )
            return None
        if "dmu" in r.keys():
            self.ped.pedigree_for_dmu()
            return None

    def call_heritability(self, r):
        try:
            r["write"]
            write = True
        except KeyError:
            write = False
        try:
            r["read"]
            read = True
        except KeyError:
            read = False
        try:
            r["required"]
            if r["required"]:
                required = r["required"]
            else:
                print("Phenotype parameters must not be empty")
                return None
        except KeyError:
            print("Please provide phenotype parameters")
            return None
        try:
            r["treshold"]
            depth = r["threshold"]
        except KeyError:
            depth = 0
        try:
            r["pmergebnisse"]
            pmergebnisse = True
        except KeyError:
            pmergebnisse = False
        try:
            r["breed"]
            breed = r["breed"]
        except KeyError:
            breed = None
        try:
            r["test"]
            test = True
        except KeyError:
            test = False
        # self.herit.pedigree_data_processing(
        #     destination_traits=required,
        #     depth=depth,
        #     selected_breed=breed,
        #     pmergebnisse=pmergebnisse,
        #     write=write,
        #     read=read,
        #     test=test
        # )

if __name__ == "__main__":
    Calls().call_function()