#!/usr/bin/env python3

import pandas as pd
import numpy as np
import re
import os
import gc
from PopulationParameters import PopulationParameters
from MultiProcessDivision.divide import divide
from RunningApplyLoops import RunningApplyLoops
from LactationCurves import LactationCurves
from Phenotype_data_corrections import Phenotype_data_corrections


class Lactation_modelling():
    def __init__(self):
        self.__pp = PopulationParameters()
        self.__ral = RunningApplyLoops()
        self.__phd = Phenotype_data_corrections()

    def lact_model(self, trait, _in=None, save=False) -> dict:
        """
        This function is basically a wrapper function to execute the RunningApplyLoops().apply_groupby() function
        with the parameter "Predict_lact_days". By running this function, the __predict_daily_yield()
        function is ultimately called and executed. 
        Problems in this function are a little tricky, since the parameter trait depends on the way 
        the function is called, when it comes to its type. If the function is invoked from the 
        Calls() class with the required arguments given in argparse style, the trait may be a list of strings 
        instead of a string. Due to the disruption, this has already caused in the 
        __predict_daily_yield() function, a loop is now required to unpack the list of strings, 
        even if the list only contains a single value. In order to follow the D(on't) R(epeat) Y(ourself)
        principle, all execution is now outsourced to a dedicated function.
        """
        if _in is None:
            _in = self.__phd.adapt_milk_recording(
                required_columns=trait,
                fixed=["BETRIEB"],
                tl305=True,
                pmonly=False,
                lact_model=False,
                write=False,
                year_season=False,
                correct_eartags=False
            )
            pattern_required = True
            pattern_priors = re.compile(r"[A-Z]{3}\ 305TL\_prior")
            _in = {x:_in[x] for x in _in.keys() if not pattern_priors.match(x)}
        r = {}
        for key, value in _in.items():
            if "pattern_required" in locals() and pattern_required is True:                 
                breed_pattern = re.compile(r"^[A-Z]{3}\ ")                 
                breed = breed_pattern.match(key).group().strip()
            else:
                breed = key
            if isinstance(trait, list):
                for _trait in trait:
                    _res = self.__runModelFunction(
                        data=value,
                        trait=_trait
                    )
                    if _res is not None:
                        r[f"{breed} {_trait}"] = _res
                        if save is True:
                            r[f"{breed} {_trait}"].to_csv(
                                f"{self.__pp.milkrecording_folder}/{breed} {_trait} DailyYieldEstimation.csv"
                            )
            elif isinstance(trait, str):
                _res = self.__runModelFunction(
                    data=value,
                    trait=trait
                )
                if _res is not None:
                    r[f"{breed} {trait}"] = _res
                if save is True:
                    r[f"{breed} {trait}"].to_csv(
                        f"{self.__pp.milkrecording_folder}/{breed} {trait} DailyYieldEstimation.csv"
                    )
        return r
    
    def __runModelFunction(self, data:pd.DataFrame, trait:str) -> pd.DataFrame:
        try:
            data = data.loc[:,trait]
        except KeyError:
            return None
        lactcurve_values = LactationCurves.legendre()
        res = self.__ral.apply_groupby(
            data=data,
            action="Predict_lact_days",
            coeffs=lactcurve_values,
            trait=trait
        )
        res.index = pd.MultiIndex.from_arrays(
            [
                res.index.get_level_values("ISO_LEBENSNR"), 
                pd.DatetimeIndex(
                    res.index.get_level_values("Parturition_date")
                )
            ], names=["ear_tag", "Parturition_date"]
        )
        return res
    
    def compare_curve_to_yield(self, trait:list, **kwargs) -> dict:
        if "recalculate" in kwargs.keys():
            recalculate = kwargs["recalculate"]
        else:
            recalculate = False
        if "write" in kwargs.keys():
            write = kwargs["write"]
        else:
            write = False
        if "read" in kwargs.keys():
            read = kwargs["read"]
        else:
            read = False
        r = {}
        if read is True:
            pattern_pmergebnisse = re.compile(r"^[A-Z]{3}pmergebnisse$")
            pattern_305tl = re.compile(r"^[A-Z]{3}305TL$")
            pattern_breed = re.compile(r"^[A-Z]{3}")
            testday_recorded = {
                pattern_breed.findall(x)[0]:pd.read_csv(
                    os.path.join(self.__pp.milkrecording_folder, x),
                    sep=" ",
                    index_col=[0,1,2],
                    header=0
                ) for x in os.listdir(self.__pp.milkrecording_folder) if pattern_pmergebnisse.match(x)
            }
            tl305_recorded = {
                pattern_breed.findall(x)[0]:pd.read_csv(
                    os.path.join(self.__pp.milkrecording_folder, x),
                    sep=" ",
                    index_col=[0,1],
                    header=0
                ) for x in os.listdir(self.__pp.milkrecording_folder) if pattern_305tl.match(x)
            }
        else:
            measured_data = self.__phd.adapt_milk_recording(
                required_columns=trait,
                tl305=True,
                pmonly=True,
                write=False,
                testday=trait,
                correct_eartags=False,
                year_season=False,
                fixed=["BETRIEB"],
                lact_model=False
            )
            pattern_pmergebnisse = re.compile(r"^[A-Z]{3}\ pmergebnisse$")
            _match_testday = list(
                filter(pattern_pmergebnisse.match, measured_data.keys())
            )
            testday_recorded = {key.split(" ")[0]:measured_data[key] for key in _match_testday}

            pattern_305tl = re.compile(r"^[A-Z]{3}\ 305TL$")
            _match_305tl = list(
                filter(pattern_305tl.match, measured_data.keys())
            )
            tl305_recorded = {key.split(" ")[0]:measured_data[key] for key in _match_305tl}
            del measured_data
            gc.collect()
        for _trait in trait:
            pattern_lact_curve = re.compile(
                r"^[A-Z]{3}\ %s\ DailyYieldEstimation.csv$"%re.escape(_trait)
            )
            # This is the same solution as applied in the Phenotype_data_corrections() class, it is 
            # derived from the following stackoverflow post:
            # https://stackoverflow.com/questions/5900683/using-variables-in-python-regular-expression/5900723#5900723
            v = list(
                filter(
                    pattern_lact_curve.match, 
                    os.listdir(self.__pp.milkrecording_folder)
                )
            )
            chunksize = 10**4
            if len(v) < 1 or recalculate is True:
                del v
                gc.collect()
                for breed, _data in tl305_recorded.items():
                    # This loop contains the calculation of daily yield for the cows included in the data set.
                    # Due to the large size of data objects, which caused problems in the previously used SLURM
                    # settings, a generator approach has been chosen for processing. Running the loop returns 
                    # pd.DataFrame() objects, with entries for each day in the respective lactations of the cows included.
                    predicted = self.lact_model(
                        trait=_trait,
                        _in={breed: _data},
                        save=write
                    )
                    _recorded = testday_recorded[breed]
                    # That's the step to rename the columns with the lactation day
                    # which is connected to each of the measures
                    divided = divide(_data, range_setter=10)
                    comp = pd.DataFrame()
                    for dset in divided:
                        """
                        This loop mainly exists for the sake of compatibility with the 
                        other condition, where the required data are not calculated but 
                        read from a saved csv file. Due to the size of that csv file and
                        to avoid memory issues, this reading is performed in chunks, which 
                        also returns an iterator. This behavior is mimicked in this loop by 
                        using the divide_df() function.
                        """
                        comparison_ = self.process_precalculated_lactations(
                            data=predicted[f"{breed} {_trait}"], 
                            recorded=_recorded, 
                            trait=_trait
                        )
                        divided.remove(dset)
                        del dset
                        gc.collect()
                        comp = pd.concat([comp, comparison_], axis=0)
                        r[f"{breed} {_trait}"] = comp
            elif len(v) >= 1 or recalculate is False:
                for _file in v:
                    breed_pattern = re.compile(r"^[A-Z]{3}")
                    breed = list(filter(breed_pattern.match, _file.split(" ")))[0]
                    _r = pd.DataFrame()
                    for _data in pd.read_csv(
                            f"{self.__pp.milkrecording_folder}/{_file}", 
                            header=0, 
                            index_col=[0,1],
                            chunksize=chunksize
                        ):
                        """
                        Using the chunksize parameter does not return a pd.DataFrame but an iterator.
                        This mechanism is used to match the mechanism used in the dynamic calculation of 
                        lactation values, which is also an iterator. The explanation on how 
                        to use this mechanism is explained in this stackoverflow post:
                        https://stackoverflow.com/questions/25962114/how-do-i-read-a-large-csv-file-with-pandas
                        """
                        _data.index = pd.MultiIndex.from_arrays(
                            [
                                _data.index.get_level_values(0), 
                                pd.DatetimeIndex(_data.index.get_level_values(1))
                            ], 
                            names=["ear_tag", "Parturition_date"]
                        )
                        """
                        This explicit casting of indexes is only necessary when dealing with DataFrames read from 
                        csv files, which come unformatted with only the index columns assigned to further processing.
                        Since the next steps rely heavily on uniformilly aligned indexes, the index has to be formatted
                        explicitly in this step.
                        """
                        _recorded = testday_recorded[breed]
                        __r = self.process_precalculated_lactations(
                            data=_data, 
                            recorded=_recorded, 
                            trait=_trait
                        )
                        _r = pd.concat([_r, __r], axis=0)
                        del __r
                        gc.collect()
                    r[f"{breed} {_trait}"] = _r
            if write is True:
                r[f"{breed} {_trait}"].to_csv(
                    os.path.join(
                        self.__pp.milkrecording_folder,
                        f"{breed} {_trait} DailyDeviation.csv"
                    ),
                    header=True,
                    index=True
                )
        return r

    def process_precalculated_lactations(
        self, 
        data: pd.DataFrame, 
        recorded: pd.DataFrame, 
        trait: str
        ) -> pd.DataFrame:
        """
        The pd.DataFrame passed with the "data" argument is the predicted yield for the 
        animals included in the dataset, while the test day records measured in reality
        are included in the "recorded" pd.DataFrame
        """
        if "MILCH_KG" in trait:
            _rv = recorded.loc[:,["MILCH_MENGE_TAG", "LAKT_TAG"]]
        elif "FETT_KG" in trait:
            _rv = recorded.loc[:,["FETTANTEIL_TAG", "LAKT_TAG"]]
        elif "EIWEISS_KG" in trait:
            _rv = recorded.loc[:,["EIWEISSANTEIL_TAG", "LAKT_TAG"]]
        elif "ZELLZAHLEN" in trait:
            _rv = recorded.loc[:,["ZELLZAHLEN", "LAKT_TAG"]]
        else:
            return None
        _rv = _rv.loc[_rv["LAKT_TAG"] < _rv["LAKT_TAG"].quantile(0.9),:]
        """
        To avoid inclusion of lactations, which are in fact two separate lactations, where a parturition in between 
        has been missed in the data, lactation lengths are calculated for each cow. Due to the nature, of this job, 
        that no other jobs depend on its execution, this step is conducted in a parallel manner in the 
        calculate_lactation_length() function.
        Invalid values are then filtered using a quantile.
        """
        d = self.__ral.apply_groupby(
            data=_rv,
            action="Comparison_measured_predicted",
            predicted=data,
            trait=trait
        )
        return d