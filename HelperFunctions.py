#!/usr/bin/env python3

import numpy as np
import pandas as pd
import random
import functools


class HelperFunctions():
    def __init__(self):
        pass
    
    def coefficient_of_relation(self, amount_of_animals):
        corr_animals = pd.DataFrame(
            columns=np.arange(amount_of_animals) + 1, 
            index=np.arange(amount_of_animals) + 1
        )
        for index in corr_animals.index:
            for col in corr_animals.columns:
                # this loop creates the matrix with the coefficients of 
                # relationship between the bulls involved
                if col == index:
                    corr_animals.loc[index, col] = 1
                elif np.isnan(corr_animals.loc[col, index]) is True:
                    # this creates a diagonally mirrowed matrix, which is necessary 
                    # as there are only a limited number of bulls in the matrix
                    corr_animals.loc[index, col] = corr_animals.loc[col, index]
                else:
                    corr_animals.loc[index, col] = self.relations(
                        common_pred=np.int(np.random.randint(3, 100, size=1)),
                        generation_dam=np.int(np.random.randint(3, 50, size=1)),
                        generation_sire=np.int(np.random.randint(3, 50, size=1)),
                        inbreedcoeff=np.float(np.random.uniform(0.0, 1.0, size=1))
                    )
                # the matrix of relationship is not to be confused with the numerator relationship matrix, 
                # which includes genetic relations among the animals themselves and follows the calculation 
                # of Henderson 1976
        return corr_animals
    
    def d_matrix(self, sdf, coeff_rel_sire=None, coeff_rel_dam=None):
        # the sire and the dam dataframes are argument to the function
        # d is the variance matrix for the gentic merit of the parents to their progeny
        if coeff_rel_dam is not None and coeff_rel_sire is not None:
            # this case calculates an animal model with all cows involved in the matrix
            r = pd.DataFrame(
                columns=np.arange(len(sdf.index)) + 1, 
                index=np.arange(len(sdf.index)) + 1,
                data=np.zeros(
                    shape=(len(sdf.index), len(sdf.index))
                )
            )
        elif coeff_rel_sire is not None and coeff_rel_dam is None:
            # this case calculates a sire model
            r = pd.DataFrame(
                columns=np.arange(len(sdf.columns)) + 1, 
                index=np.arange(len(sdf.columns)) + 1, 
                data=np.zeros(shape=(len(sdf.columns), len(sdf.columns)))
            )
        else:
            return None
        n = 1
        for bull in sdf.columns:
            coeff_rel_sire_single = coeff_rel_sire.loc[
                bull, 
                np.int(
                    random.choice(np.array(coeff_rel_sire.columns).tolist())
                )
            ]
            # the bull's coefficient of relation is determined from an own table with 
            # coefficients of relation
            
            cows = sdf[bull].replace(0, np.NaN)
            cows.dropna(inplace=True, how="any")
            if coeff_rel_dam is not None:
                for cow in cows.index:
                    coeff_rel_dam_single = coeff_rel_dam.loc[
                        cow, 
                        np.int(
                            random.choice(
                                np.array(coeff_rel_dam.columns).tolist()
                            )
                        )
                    ]
                    # the coefficient of relation for the dam is chosen randomly, by chosing the parent involved from the 
                    # matrix of relationship. 
                    r.loc[n, n] = 1 - (0.25 * (coeff_rel_sire_single + coeff_rel_dam_single))
                    # in the matrix of kinship, only the animals of interest are involved, not the tested sires
            else:
                r.loc[n, n] = 1 - (0.25 * (coeff_rel_sire_single + 1))
                # the diagonal element is calculated, as if the dam is unknown
            n += 1
        return r
    
    def t_matrix(self, amount_of_animals, base_animals=2):
        mmatrix = pd.DataFrame(
            columns=np.arange(amount_of_animals) + 1, 
            index=np.arange(amount_of_animals) + 1, 
            data=np.zeros(shape=(amount_of_animals, amount_of_animals)))
        for animal in mmatrix.columns:
            if animal - base_animals < 1:
                # the condition allows to set the amount of animals, 
                # for which no parents are included into the matrix
                continue
            else:
                parent1 = animal - 1
                parent2 = animal - 2
                mmatrix.loc[animal, parent1] = 0.5
                # this line makes the assumption, that 50 % of the animal's 
                # total genetic merit comes from a single parent
                # in this case, both parents are known, as they are set to 1
                mmatrix.loc[animal, parent2] = 0.5
        i = pd.DataFrame(
            columns=mmatrix.columns, 
            index=mmatrix.index, 
            data=np.identity(
                len(mmatrix.index)
            )
        )
        return pd.DataFrame(
            data=(
                i.values - mmatrix.values
            ), 
            columns=mmatrix.columns, 
            index=mmatrix.index
        )
        # the return value is already the invert of the t matrix
    
    def a_inverse(self, sdf, coeff_rel_sires, coeff_rel_dams=None, base_animals=2):
        d_matrix = self.d_matrix(sdf, coeff_rel_sires, coeff_rel_dams)
        print(d_matrix)
        if d_matrix is None:
            return None
        t_matrix = self.t_matrix(len(d_matrix.index), base_animals)
        a_inverse = t_matrix.T.values * np.linalg.inv(d_matrix.values) * t_matrix.values
        a_inverse = pd.DataFrame(
            columns=np.arange(len(d_matrix.columns)) + 1, 
            index=np.arange(len(d_matrix.index)) +1, 
            data=a_inverse
        )
        return a_inverse #.round(3)
        # the returned dataframe is rounded to 3 decimals
    
    @functools.lru_cache(20)
    # caches the results of the following function
    def relations(self, common_pred, generation_dam, generation_sire, inbreedcoeff):
        return np.sum(
            np.power(
                0.5, 
                generation_dam + generation_sire + 1
            ) * (1 + inbreedcoeff) for x in range(1, common_pred + 1)
        )

