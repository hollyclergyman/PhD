#!/usr/bin/env python3

from SheepData import SheepData
import multiprocessing as mp
import itertools
import argparse
import numpy as np
import pandas as pd

class HyperparameterTuning():
    def __init__(self):
        """
        This class serves the optimization of the hyperparameter to improve the 
        model accuracy. Therefore, the testAlphas() function iterates chunks of 
        the alpha
        """
        self.__shd = SheepData()

    def createSupervisor(self, write=True):
        parser = argparse.ArgumentParser(
            description="Hyperparameter tuning to optimize LASSO model"
        )
        parser.add_argument("-rg", "--range", type=str, nargs="?")
        _parser = vars(parser.parse_args())
        r = {k:v for k,v in _parser.items() if v is not None}
        if "range" in r.keys():
            try:
                alphas = np.arange(np.int(r["range"]))[::-1]/np.int(np.ceil(np.int(r["range"])/1000) * 1000)
                print("Worked")
            except ValueError:
                alphas = np.arange(10000)[::-1]/1000
        else:
            alphas = np.arange(10000)[::-1]/1000
        # This is the shortened way of writing that stuff, inspiration taken 
        # from various stackoverflow posts, such as this one:
        # https://stackoverflow.com/questions/29806239/how-can-i-flip-the-order-of-a-1d-numpy-array/29806292#29806292
        divided = self.divide_array(alphas)
        r = pd.DataFrame()
        with mp.Pool(mp.cpu_count()) as pool:
            l = [v for v in pool.map(self.testAlphas, divided) if v is not None]
            _r = pd.concat(l, axis=1)
            r = pd.concat([r, _r], axis=1)
        r.columns = pd.MultiIndex.from_tuples(
            r.columns, names=["Alpha", "Compressed", "Measures"]
        )
        if write is True:
            r.to_csv(f"{self.__shd.sheep_folder}/Hyperparameter_Results.csv")
        return r

    def divide_array(self, data):
        l = []
        if float(len(data)/mp.cpu_count()).is_integer():
            chunklength = len(data)/mp.cpu_count()
            i = 0
            for x in range(mp.cpu_count()):
                start = np.int64(i * chunklength)
                stop = np.int64((i + 1) * chunklength - 1)
                if i != 0:
                    l.append(data[start - 1:stop])
                else:
                    l.append(data[start:stop])
                i += 1
        else:
            chunklength = np.ceil(len(data)/mp.cpu_count())
            i = 0
            for x in range(mp.cpu_count()):
                if len(l) < mp.cpu_count() - 1:
                    start = np.int64(i * chunklength)
                    stop = np.int64((i + 1) * chunklength - 1)
                else:
                    start = np.int64(i * chunklength)
                    stop = len(data)
                if i != 0:
                    l.append(data[start - 1:stop])
                else:
                    l.append(data[start:stop])
                i += 1
        return l

    def testAlphas(self, alphas):
        r = pd.DataFrame()
        for alpha in alphas:
            d = self.__shd.determine_deviation(
                selected_model="lasso",
                test_ratio=0.2, 
                sort_by_breed=False, 
                alpha=alpha
            )
            if d is None:
                continue
            u_squared_root_deviation = d["Uncompressed Deviation"]
            u_squared_root_deviation.columns = pd.MultiIndex.from_arrays(
                [
                    list(itertools.repeat(alpha, len(u_squared_root_deviation.columns))),
                    list(itertools.repeat("Uncompressed", len(u_squared_root_deviation.columns))),
                    u_squared_root_deviation.columns
                ], names=["Alpha", "Compressed", "Measures"]
            )
            c_squared_root_deviation = d["Compressed Deviation"]
            c_squared_root_deviation.columns = pd.MultiIndex.from_arrays(
                [
                    list(itertools.repeat(alpha, len(c_squared_root_deviation.columns))),
                    list(itertools.repeat("Compressed", len(c_squared_root_deviation.columns))),
                    c_squared_root_deviation.columns
                ], names=["Alpha", "Compressed", "Measures"]
            )
            if len(r.columns) < 1:
                r = pd.concat([r, u_squared_root_deviation], axis=1)
                r = pd.concat([r, c_squared_root_deviation], axis=1)
            else:
                last_alpha = np.unique(r.columns.get_level_values("Alpha"))[-1]
                for measure in np.unique(u_squared_root_deviation.columns.get_level_values("Measures")):
                    _values = u_squared_root_deviation.loc[:,pd.IndexSlice[:,:,measure]].dropna()
                    _values.name = alpha
                    comparison = r.loc[:,pd.IndexSlice[last_alpha, "Uncompressed", measure]].dropna().iloc[0]
                    # This, great advice, which solves quite a bunch of problems I ever encountered 
                    # in my endless fight agains loc[pd.IndexSlice[:],:] was given in the following 
                    # stackoverflow post:
                    # https://stackoverflow.com/questions/33246771/convert-pandas-data-frame-to-series/33247007#33247007
                    #comparison.name = last_alpha
                    compared = _values < comparison
                    _mean = compared.mean()
                    if _mean.loc[pd.IndexSlice[alpha,"Uncompressed",measure]] != np.float(1):
                        _values.name = (alpha, "Uncompressed" ,measure)
                        r = pd.concat([r, _values], axis=1)
                        r.columns = pd.MultiIndex.from_tuples(
                            r.columns, names=["Alpha", "Compressed", "Measures"]
                        )
                    else:
                        continue
                for _compressed in np.unique(c_squared_root_deviation.columns.get_level_values("Measures")):
                    _val = c_squared_root_deviation.loc[:,pd.IndexSlice[:,:,measure]].dropna()
                    _compare = r.loc[:,pd.IndexSlice[last_alpha, "Compressed", measure]].dropna().iloc[0]
                    _compared = _val < _compare
                    __mean = _compared.mean()
                    if __mean.loc[pd.IndexSlice[alpha,"Compressed",measure]] != np.float(1):
                        _val.name = (alpha, "Compressed" ,measure)
                        r = pd.concat([r, _val], axis=1)
                        r.columns = pd.MultiIndex.from_tuples(
                            r.columns, names=["Alpha", "Compressed", "Measures"]
                        )
                    else:
                        continue
        return r
                
if __name__ == "__main__":
    HyperparameterTuning().createSupervisor()
