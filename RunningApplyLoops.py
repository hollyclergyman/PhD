#!/usr/bin/env python3

import pandas as pd
import numpy as np
import multiprocessing as mp
from functools import partial
import gc
from MultiProcessDivision.divide import divide
from pandas.core.indexing import IndexSlice

class RunningApplyLoops():
    def __init__(self):
        pass
    
    def apply_groupby(self, data:pd.DataFrame, **kwargs) -> pd.DataFrame:
        """
        Permitted kwargs["action"] are:
        Test_day -> Counts the test days during a lactation
        Cow_age -> calculates the cow's age at a lactation
        Lactation_curve -> Adds the lactation curve's percentage values to the respective lactation
        Daily_yields -> Adds raw values from test day records to the respective lactation
        Lactation_day -> Calculates the lactation day for each entry in test day records
        Predict_lact_days -> Calculates the projected daily yield according to the function passed in coeffs
        """
        splitted = divide(
            pd.Series(data=np.unique(data.index.get_level_values(0))), series=True
        )
        try:
            _splitted = [data.loc[pd.IndexSlice[_x.to_numpy(),:],:] for _x in splitted]
        except TypeError:
            _splitted = [data.loc[_x.to_numpy(),:] for _x in splitted]
        del splitted
        gc.collect()
        r = pd.Series()
        with mp.Pool(processes=mp.cpu_count()) as pool:
            if "Test_day" in kwargs["action"]:
                to_execute = partial(
                    self.run_groupby, action="Test_day"
                )
            if "Cow_age" in kwargs["action"] and "pedigree" in kwargs.keys():
                to_execute = partial(
                    self.run_groupby, action="Cow_age", pedigree=kwargs["pedigree"]
                )
            if "Lactation_curve" in kwargs["action"] and "lactation" in kwargs.keys() and "test_days" in kwargs.keys():
                to_execute = partial(
                    self.run_groupby, 
                    action="Lactation_curve", 
                    lactation=kwargs["lactation"], 
                    test_days=kwargs["test_days"]
                )
            if "Daily_yields" in kwargs["action"] and "test_days" in kwargs.keys():
                to_execute = partial(
                    self.run_groupby,
                    action="Daily_yields",
                    test_days=kwargs["test_days"],
                    trait=kwargs["trait"]
                )
            if "Lactation_day" in kwargs["action"]:
                to_execute = partial(
                    self.run_groupby, action="Lactation_day"
                )
            if "Predict_lact_days" in kwargs["action"] and "coeffs" in kwargs.keys():
                to_execute = partial(
                    self.run_groupby,
                    action="Predict_lact_days",
                    coeffs=kwargs["coeffs"],
                    trait=kwargs["trait"]
                )
            if "Comparison_measured_predicted" in kwargs["action"] and "predicted" in kwargs.keys():
                to_execute = partial(
                    self.run_groupby,
                    action="Comparison_measured_predicted",
                    predicted=kwargs["predicted"],
                    trait=kwargs["trait"]
                )
            _r = pd.concat(
                [i for i in pool.map(to_execute, _splitted) if i is not None], 
                axis=0
            )
            r = pd.concat([r, _r], axis=0)
            del _r
            gc.collect()
        if "Test_day" in kwargs["action"] or "Lactation_day" in kwargs["action"]:
            r.index = pd.MultiIndex.from_tuples(
                r.index, 
                names=["ISO_LEBENSNR", "Parturition_date", "Test_day"]
            )
        else:
            r.index = pd.MultiIndex.from_tuples(
                r.index,
                names=["ISO_LEBENSNR", "Parturition_date"]
            )
        return r

    def run_groupby(self, data:pd.DataFrame, **kwargs) -> pd.DataFrame:
        """
        The action keywords passed in the the kwargs are the same as for the calling 
        function. Passing such keywords allows to run the same function for multiple, 
        varying causes.
        """
        if "Test_day" in kwargs["action"]:
            data = data.groupby(
                [
                    data.index.get_level_values(0),
                    data.index.get_level_values(1)
                ]
            ).apply(lambda x: self.__counting(x))
            return data["TEST_TAG"]
        if "Cow_age" in kwargs["action"]:
            data = data.groupby(
                [
                    data.index.get_level_values(0),
                    data.index.get_level_values(1)
                ]
            ).apply(lambda cowdata: self.__cows_age(cowdata, kwargs["pedigree"]))
            return data
        if "Lactation_curve" in kwargs["action"]:
            data = data.groupby(
                [
                    data.index.get_level_values(0),
                    data.index.get_level_values(1)
                ]
            ).apply(lambda cowdata: self.__adding_lactation_curve(
                    data=cowdata, lactation=kwargs["lactation"], test_days=kwargs["test_days"]
                )
            )
            return data
        if "Daily_yields" in kwargs["action"]:
            data = data.groupby(
                [
                    data.index.get_level_values(0),
                    data.index.get_level_values(1)
                ]
            ).apply(
                lambda cowdata: self.__testday_to_whole_lactation(
                    data=cowdata, test_days=kwargs["test_days"], trait=kwargs["trait"]
                )
            )
            return data
        if "Predict_lact_days" in kwargs["action"]:
            res = data.groupby(
                [
                    data.index.get_level_values(0),
                    data.index.get_level_values(1)
                ]
            ).apply(
                lambda prediction: self.__predict_daily_yield(
                    prediction, 
                    coeffs=kwargs["coeffs"],
                    trait=kwargs["trait"]
                )
            )
            _res = pd.DataFrame(
                index=np.unique(res.index.get_level_values(2))
            )
            grouped = res.groupby(
                [
                    res.index.get_level_values(0), 
                    res.index.get_level_values(1)
                ]
            )
            for cpart, _data in grouped:
                cpart_d = pd.Series(
                    data=_data.to_numpy(),
                    name=cpart,
                    index=np.unique(res.index.get_level_values(2))
                )
                _res = pd.concat([_res, cpart_d], axis=1)
                del cpart_d, _data
                gc.collect()
            _res = _res.T
            _res.index = pd.MultiIndex.from_tuples(
                _res.index,
                names=["ISO_LEBENSNR", "Parturition_date"]
            )
            return _res
        if "Comparison_measured_predicted" in kwargs["action"]:
            res = data.groupby(
                [
                    data.index.get_level_values(0),
                    data.index.get_level_values(1)
                ]
            ).apply(
                lambda _measured: self.__compare_prediction_to_measured(
                    measured=_measured,
                    predicted=kwargs["predicted"],
                    trait=str(kwargs["trait"])
                )
            )
            return res
        
    def __counting(self, data:pd.DataFrame) -> pd.DataFrame:
        data["TEST_TAG"] = np.arange(1, len(data.index) + 1, dtype=np.uint8)
        return data

    def __cows_age(self, data:pd.DataFrame, pedigree:pd.DataFrame) -> pd.DataFrame:
        cow = np.unique(data.index.get_level_values(0))[0]
        parturition = np.unique(data.index.get_level_values(1))[0]
        if cow in pedigree.index:
            data.loc[:,"Age"] = pd.to_datetime(parturition, errors="coerce").year - pedigree.loc[cow, "Year of Birth"]
        else:
            data.loc[:,"Age"] = 0
        return data

    def __adding_lactation_curve(
        self, 
        data:pd.DataFrame,  
        test_days:pd.DataFrame, 
        lactation:pd.DataFrame) -> pd.DataFrame:
        cow = np.unique(data.index.get_level_values(0))[0]
        parturition = np.unique(data.index.get_level_values(1))[0]
        if cow in test_days.index.get_level_values(0):
            cow_test_days = test_days.loc[pd.IndexSlice[cow,:],:]
            if parturition in cow_test_days.index.get_level_values(1):
                lact_test_days = test_days.loc[pd.IndexSlice[cow, parturition],"LAKT_TAG"]
                for test, test_day in enumerate(lact_test_days, 1):
                    try:
                        data.loc[pd.IndexSlice[cow, parturition], test] = lactation.loc[
                            test_day, "Coefficient"
                        ]
                    except KeyError:
                        continue
            return data
        else:
            return data
    
    def __testday_to_whole_lactation(
        self, data:pd.DataFrame, test_days:pd.DataFrame, trait:str
    ) -> pd.DataFrame:
        _data = pd.DataFrame(columns=data.columns.to_list() + [trait])
        if np.unique(data.index.get_level_values(0))[0] in test_days.index.get_level_values(0):
            cdata = test_days.loc[
                pd.IndexSlice[np.unique(data.index.get_level_values(0))[0],:,:],:
            ]
            print(cdata)
            if np.unique(data.index.get_level_values(1))[0] in cdata.index.get_level_values(1):
                lactation = cdata.loc[pd.IndexSlice[:,np.unique(data.index.get_level_values(1))[0],:],:]
                for testday in np.unique(lactation.index.get_level_values(2)):
                    _data.loc[testday,data.columns] = data.to_numpy()
                    _data.loc[testday,trait] = lactation.loc[pd.IndexSlice[:,:,testday],trait].to_numpy()[0]
                _data.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat(np.unique(data.index.get_level_values(0))[0],_data.index.size),
                        np.repeat(np.unique(data.index.get_level_values(1))[0],_data.index.size)
                    ], names=["ear_tag", "parturition_date"]
                )
            else:
                data.loc[:,trait] = 0
                return data
        else:
            data.loc[:,trait] = 0
            return data
        # elif trait in test_days.columns:
        #     trait_data = test_days.loc[pd.IndexSlice[cow, parturition], trait]
        #     for test_day_number, test_date in enumerate(trait_data.index, 1):
        #         data.loc[pd.IndexSlice[cow, parturition],f"{trait}_{test_day_number}"] = trait_data.loc[test_date]
        return _data

    def __predict_daily_yield(
        self, 
        data, 
        coeffs:pd.DataFrame,
        trait:str
        ) -> pd.Series:
        if isinstance(data, pd.DataFrame) and trait in data.columns:
            total = data.loc[:,trait].to_numpy()[0]
        else:
            total = data.to_numpy()[0]
        r = pd.Series(
            data=coeffs.loc[:,"Percentage"]*total,
            index=coeffs.index,
            name=(
                np.unique(data.index.get_level_values(0))[0],
                np.unique(data.index.get_level_values(1))[0]
            )
        )
        return r

    def __compare_prediction_to_measured(
        self,
        measured:pd.DataFrame,
        predicted:pd.DataFrame,
        trait:str
    ) -> pd.Series:
        """
        The function is written a little odd, since it iterates the pd.DataFrame 
        object, which is proven to be smaller than the other passed pd.DataFrame.
        But, there is a core reason behind this idea, which originates from the assumption, 
        that measured values provide a lower quantity of individual values than 
        predicted values. Therefore, the dataset of interest is the "measured" one, 
        while the "predicted" one only upholds to be parse aside.
        """
        print(trait, measured, sep="\n")
        if "MILCH_KG" in trait:
            column = "MILCH_MENGE_TAG"
        elif "FETT_KG" in trait:
            column = "FETTANTEIL_TAG"
        elif "ZELLZAHLEN" in trait:
            column = "ZELLZAHLEN"
        print(predicted.loc[
                    pd.IndexSlice[
                        np.unique(measured.index.get_level_values(0)[0]),
                        np.unique(measured.index.get_level_values(1)[0])
                    ],
                    measured.loc[
                        pd.IndexSlice[
                            np.unique(measured.index.get_level_values(0))[0],
                            np.unique(measured.index.get_level_values(1))[0]
                        ],:
                ]].to_numpy()
        )
        r = pd.Series(
            name=(
                np.unique(measured.index.get_level_values(0))[0],
                np.unique(measured.index.get_level_values(1))[0]
            ),
            index=np.unique(
                measured.loc[
                    pd.IndexSlice[
                        np.unique(measured.index.get_level_values(0))[0],
                        np.unique(measured.index.get_level_values(1))[0]
                    ],"LAKT_TAG"
                ]
            ),
            data=np.square(
                measured.loc[
                    pd.IndexSlice[
                        np.unique(measured.index.get_level_values(0))[0],
                        np.unique(measured.index.get_level_values(1))[0]
                    ],column
                ].to_numpy() - predicted.loc[
                    pd.IndexSlice[
                        np.unique(measured.index.get_level_values(0)[0]),
                        np.unique(measured.index.get_level_values(1)[0])
                    ],
                    measured.loc[
                        pd.IndexSlice[
                            np.unique(measured.index.get_level_values(0))[0],
                            np.unique(measured.index.get_level_values(1))[0]
                        ],"LAKT_TAG"
                ]].to_numpy()
            )
        )
        return r
