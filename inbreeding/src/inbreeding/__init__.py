from .Inbreeding import Inbreeding
from .MissingYOB import MissingYOB
from .RelationAnimals import RelationAnimals

"""
The advice for this format in the __init__.py file was given in the following 
thread: 
https://stackoverflow.com/questions/45122/python-packages-import-by-class-not-file/45126#45126
And even though it is focussed on Python2, it still seems legit in Python3
"""

__version__ = "0.0.1" 
__author__ = "Thomas Rahimi"
__credits__ = "FG Tierzucht, Universität Kassel"