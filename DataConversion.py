#!/usr/bin/env python3

from PopulationParameters import PopulationParameters
import pandas as pd
import numpy as np


class DataConversion():
    def __init__(self, pedigree_folder=None):
        self.pp = PopulationParameters()
        if pedigree_folder is None
            self.pedigree_folder = self.pp.pedigree_folder
        else:
            self.pedigree_folder = pedigree_folder

    def pedigree_data(self):
        r = self.pp.pedigree_data()
        for k, v in r.items():
            v.to_csv(
                self.pedigree_folder + k + ".csv"
            )

if __name__ == "__main__":
    DataConversion().pedigree_data()
