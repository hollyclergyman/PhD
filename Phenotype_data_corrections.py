#!/usr/bin/env python3

import numpy as np
import pandas as pd
import re
import gc
import multiprocessing as mp
from PopulationParameters import PopulationParameters
from MultiProcessDivision.divide import divide
from LactationCurves import LactationCurves
from Pedigree import Pedigree
from functools import partial
from RunningApplyLoops import RunningApplyLoops


class Phenotype_data_corrections():
    def __init__(self):
        self.pp = PopulationParameters()
        self.pedigree = Pedigree()
        self.ral = RunningApplyLoops()
        
    def evaluations(self, data=None, passed=False):
        if data is None:
            data = self.pp.read_milkrecording()
        pattern_305tl = re.compile(r"[A-Z]{1,3}\ 305TL")
        # This pattern definition is necessary to allow running the function in cases, where
        # the breed is not RDN
        if data.keys():
            res = {}
        for key, value in data.items():
            _data = pd.DataFrame()
            for column in np.unique(value.columns):
                try:
                    dset = value[column].astype(np.float64)
                    _data = pd.concat([_data, dset], axis=1)
                except ValueError:
                    _data = pd.concat([_data, value[column]], axis=1)
                except TypeError:
                    _data = pd.concat([_data, value[column]], axis=1)
            if pattern_305tl.match(key):
                # The function is run in different situations, among which one provides a dictionary with 
                # datasets from multiple breeds. If this case occurs, the control flow is altered to run 
                # the function multiple times.
                _pedigree = pd.read_csv(f"{self.pp.pedigree_folder}Pedigree RDN.csv", index_col=[0,1])
                # the pedigree is more of a backup, in case pedigree information, such as 
                # year of birth is required
                traits_to_check = [
                    'MILCH_KG', 
                    'FETT_KG', 
                    'EIWEISS_KG',
                    'VERZOEGERUNGSZEIT', 
                    'GUESTZEIT', 
                    'RASTZEIT', 
                    'TRAECHTIGKEITSDAUER'
                ]
                l = []
                if any(x in _data.columns for x in traits_to_check) is True:
                    for col in _data.columns:
                        if col in traits_to_check:
                            l.append(col)
                else:
                    if passed is True:
                        res[key] = None
                    else:
                        res[key] = {"data": None, "parameters": None}
                        continue
                qc = pd.DataFrame()
                _max = _data.loc[:,l].max(axis=0)
                _min = _data.loc[:,l].min(axis=0)
                _mean = _data.loc[:,l].mean(axis=0)
                _std = _data.loc[:,l].std(axis=0)
                _max.name = "max"
                _min.name = "min"
                _mean.name = "mean"
                _std.name = "std"
                qc = pd.concat([qc, _max], axis=1)
                qc = pd.concat([qc, _min], axis=1)
                qc = pd.concat([qc, _mean], axis=1)
                qc = pd.concat([qc, _std], axis=1)
                qc = qc.T
                for x in range(90, 100):
                    v = _data.loc[:,l].quantile(x/100, axis=0).values
                    # these are the values, which have the respective amount of values beneath them
                    # since the quantile of 90 looks reasonable for fertility values, all values being higher
                    # than those of quantile 90 are discarded
                    qc.loc[f"Quantile {x}",:] = v
                for _x in range(1, 11):
                    _v = _data.loc[:,l].quantile(_x/100, axis=0).values
                    qc.loc[f"Quantile {_x}",:] = _v
                if passed is False:
                    res[key] = {"data":value, "parameters":qc}
                if passed is True:
                    return {"data":value, "parameters":qc}
                    """
                    This return statement is actually not for running the function in the 
                    originally intended way to quality check all data. Instead, its purpose 
                    is to quality check data, passed to the function as a parameter. 
                    Therefore, the function actually only returns the quality checked data, which 
                    is the "data" field in the dictionary and the parameters used for this quality 
                    assessment.
                    """
        if res.keys() and passed is False:
            return res
                #return qc

    def data_cleaning(self, data, breed=None) -> pd.DataFrame:
        if not isinstance(data, dict) and breed is not None:
            _in = self.evaluations({f"{breed} 305TL":data}, passed=True)
        elif not isinstance(data, dict) and breed is None:
            _in = self.evaluations({f"UNB 305TL":data}, passed=True)
        else:
            _in = self.evaluations(data, passed=True)
        _data = _in["data"]
        if _data is None:
            return None
        # this is the data part of the processing step in the evaluations
        # function
        self.__parameters = _in["parameters"]
        # this is a class variable to allow access from the processing function 
        # to the values stored in it
        factors = ['VERZOEGERUNGSZEIT', 'GUESTZEIT', 'RASTZEIT', "TRAECHTIGKEITSDAUER"]
        """
        These factors are the ones where outliers are the most likely to occur, since 
        wrong data entries can easily be made by skipping parturations or inseminations.
        """
        # Hint given by:
        # https://stackoverflow.com/questions/10867028/get-pandas-read-csv-to-read-empty-values-as-empty-string-instead-of-nan/11005208#11005208
        requiring_columns = ["GUESTZEIT", "RASTZEIT", "VERZOEGERUNGSZEIT"]
        if any(x in _data.columns for x in factors) is True:
            _data.fillna(0, inplace=True)
            _data.reset_index(drop=False, inplace=True)
            for _factor in factors:
                if _factor in _data.columns:
                    _data[_factor] = _data[_factor].astype(np.float)
                    if _factor in requiring_columns and "GUESTZEIT" not in _factor:
                        split = divide(_data[[_factor, "ERSTE_BELEGUNG"]])
                    elif _factor in requiring_columns and "GUESTZEIT" in _factor:
                        split = divide(_data[[_factor, "BELEGDATUM_FUER_KALB"]])
                    else:
                        split = divide(_data[_factor], series=True)
                    with mp.Pool(processes=mp.cpu_count()) as pool:
                        if "verzoegerungszeit" in _factor.lower():
                            """
                            time between first insemination and first pregnancy day
                            """
                            _res = pd.concat(
                                pool.map(self.verzoegerung, split), axis=0, sort=False
                            )
                            _data.drop(_factor, axis=1, inplace=True)
                            _data = pd.concat([_data, _res], axis=1)
                        if "guestzeit" in _factor.lower():
                            """
                            time between parturition and first pregnancy day
                            """
                            _res = pd.concat(
                                pool.map(self.guest, split), axis=0, sort=False
                            )
                            _data.drop(
                                [_factor, "BELEGDATUM_FUER_KALB"], axis=1, inplace=True
                            )
                            _data = pd.concat([_data, _res], axis=1)
                        if "rastzeit" in _factor.lower():
                            """
                            time between parturition and first insemination
                            """
                            _res = pd.concat(
                                pool.map(self.rast, split), axis=0, sort=False
                            )
                            _data.drop([_factor, "ERSTE_BELEGUNG"], axis=1, inplace=True)
                            _data = pd.concat([_data, _res], axis=1)
                        if "traechtigkeitsdauer" in _factor.lower():
                            _res = pd.concat(
                                pool.map(self.trächtigkeit, split), axis=0, sort=False
                            )
                            _data.drop(_factor, axis=1, inplace=True)
                            _data = pd.concat([_data, _res], axis=1)
                else:
                    continue
            _data.index = pd.MultiIndex.from_arrays(
                [_data["ISO_LEBENSNR"], _data["Parturition_date"]],
                names=["ISO_LEBENSNR", "Parturition_date"]
            )
            _data.drop(["ISO_LEBENSNR","Parturition_date"], axis=1, inplace=True)
            return _data
        else:
            return _data
    
    def verzoegerung(self, data:pd.DataFrame) -> pd.DataFrame:
        processed = data[['VERZOEGERUNGSZEIT', "ERSTE_BELEGUNG"]].apply(
            lambda x: self.__working_function(x, 'VERZOEGERUNGSZEIT'), axis=1
        )
        orig = data.drop(['VERZOEGERUNGSZEIT', "ERSTE_BELEGUNG"], axis=1)
        _data = pd.concat([orig, processed], axis=1)
        return _data
    
    def guest(self, data:pd.DataFrame) -> pd.DataFrame:
        processed = data[["GUESTZEIT", "BELEGDATUM_FUER_KALB"]].apply(
            lambda x: self.__working_function(x, "GUESTZEIT"), axis=1
        )
        orig = data.drop(["GUESTZEIT", "BELEGDATUM_FUER_KALB"], axis=1)
        _data = pd.concat([orig, processed], axis=1)
        return _data

    def trächtigkeit(self, data:pd.DataFrame) -> pd.DataFrame:
        processed = data.apply(
            lambda x: self.__working_function(x, "TRAECHTIGKEITSDAUER")
        )
        #orig = data.drop(["TRAECHTIGKEITSDAUER"], axis=1)
        #_data = pd.concat([orig, processed], axis=1)
        return processed
    
    def rast(self, data:pd.DataFrame) -> pd.DataFrame:
        processed = data[['RASTZEIT', "ERSTE_BELEGUNG"]].apply(
            lambda x: self.__working_function(x, "RASTZEIT"), axis=1
        )
        orig = data.drop(['RASTZEIT', "ERSTE_BELEGUNG"], axis=1)
        _data = pd.concat([orig, processed], axis=1)
        return _data
    
    def __working_function(self, data, parameter):
        try:
            upper_threshold = self.__parameters.loc["Quantile 90", parameter]
            lower_threshold = self.__parameters.loc["Quantile 10", parameter]
            """
            The threshold is set to a relative value concerning the complete dataset.
            Since outlier values can be deviating by far from the arithmethic mean of the 
            whole dataset, statistical measures, such as standard deviation are not suitable 
            to identify outliers. Therefore, the quantile, which is a measure for the relative
            amount of values below the set figure, which depicts a percentage value, has been chosen
            as a measure for data quality.
            """
        except KeyError:
            return np.NaN
        if isinstance(data, pd.Series):
            if np.float(data.loc[parameter]) < upper_threshold and np.float(data.loc[parameter]) >= lower_threshold:
                return data
            else:
                if "GUESTZEIT" in parameter:
                    data.loc["BELEGDATUM_FUER_KALB"] = np.NaN
                else:
                    data.loc["ERSTE_BELEGUNG"] = np.NaN
                data.loc[parameter] = np.NaN
                return data
        else:
            if np.float(data) < upper_threshold and np.float(data) >= lower_threshold:
                return data
            else:
                return np.NaN
    
    def __feature_selection(self, data:pd.DataFrame, **kwargs) -> pd.DataFrame:
        """
        This function mainly reorganizes the dataframes included to match the general way with the 
        first three lactations included into the calculation.
        """
        r = pd.DataFrame()
        split = divide(data, axis=0)
        with mp.Pool(processes=mp.cpu_count()) as pool:
            _function = partial(
                self.process_lactations, 
                single=kwargs["single"], 
                adapt_eartags=kwargs["correct_eartags"],
                columns=kwargs["columns"]
            )
            res = pd.concat(pool.map(_function, split), axis=0)
            #res.index = res.loc[:,"ISO_LEBENSNR"]
            # since the repeated measurements are now in the columns, there is no need to have 
            # a multi-level index to achieve unique identification of values
            r = pd.concat([r, res], axis=0, sort=False)
            del res
            gc.collect()
        r.fillna(0, inplace=True)
        #r.index = pd.MultiIndex.from_tuples(r.index, names=["ISO_LEBENSNR", "Parturition_date"])
        return r
    
    def process_lactations(self, data:pd.DataFrame, **kwargs) -> pd.DataFrame:
        """
        This is the worker function to select the appropriate lactations and to put them in the columns
        of the output file
        """
        if not isinstance(data.index, pd.MultiIndex):
            # the condition is sourced from this stackoverflow thread:
            # https://stackoverflow.com/questions/21081042/detect-whether-a-dataframe-has-a-multiindex/21081062#21081062
            data.index = pd.MultiIndex.from_arrays(
                [data["ISO_LEBENSNR"],data["Parturition_date"]],
                names=["ISO_LEBENSNR", "Parturition_date"]
            )
            data.drop(["ISO_LEBENSNR", "Parturition_date"], axis=1, inplace=True)
        if "single" in kwargs.keys() and kwargs["single"] is True:
            d = data
            del data
            gc.collect()
        else:
            d = data.groupby(
                    data.index.get_level_values(0)
                ).apply(lambda x: self.__reorder_data(x))
            del data
            gc.collect()
        d.fillna(0, inplace=True)
        for c in d.columns:
            if c in self.datetime_columns:
                d[c] = pd.to_datetime(d[c], errors="ignore")
            else:
                try:
                    d[c] = d[c].astype(np.float64)
                except TypeError:
                    continue
        d.replace(0, np.NaN, inplace=True)
        if "columns" in kwargs.keys() and isinstance(kwargs["columns"], np.ndarray):
            for _c in d.columns:
                try:
                    name = _c.split("_")[0]
                except IndexError:
                    name = _c
                nans = d[_c].isna().sum()
                if np.ceil(len(d[_c].index) * 0.5) < nans and name not in kwargs["columns"]:
                    d.drop(_c, axis=1, inplace=True)
                if name in kwargs["columns"] and nans == len(d[_c].index):
                    d.drop(_c, axis=1, inplace=True)
                else:
                    continue
        if "single" in kwargs.keys() and kwargs["single"] is True:
            d.index = pd.MultiIndex.from_tuples(
                d.index, 
                names=["ISO_LEBENSNR", "Parturition_date"]
            )
        else:
            d.index = pd.MultiIndex.from_arrays(
                [d.index, d["Parturition_date"]],
                names=["ISO_LEBENSNR", "Parturition_date"]
            )
            d.drop("Parturition_date", axis=1, inplace=True)
        return d

    def __reorder_data(self, data:pd.DataFrame) -> pd.DataFrame:
        _data = data.reset_index(drop=False)
        cols = [x for x in _data.columns if "ISO_LEBENSNR" not in x]
        del _data
        gc.collect()
        d = pd.Series(
            index=cols + [f"{x}_1" for x in cols] + [f"{x}_2" for x in cols],
            name=np.unique(data.index.get_level_values(0))[0]
        )
        """
        The rational behind this naming of columns is to separate data of an animal for the first 
        three lactations of its life. Since the ear tag is part of the index and is assumed to never
        change, it is not processed at this point of reorganization.
        """
        data = data[~data.index.duplicated(keep="first")]
        i = 1
        for lact in np.unique(data.index.get_level_values(1)):
            if i == 1:
                _values = data.loc[pd.IndexSlice[:, lact],:]
                _values.reset_index(inplace=True, drop=False)
                for k in _values.columns:
                    if k not in "ISO_LEBENSNR":
                        d.loc[k] = _values.loc[:,k].to_numpy()[0]
            else:
                _values = data.loc[pd.IndexSlice[:, lact],:]
                _values.reset_index(inplace=True, drop=False)
                for _k in _values.columns:
                    if _k not in "ISO_LEBENSNR":
                        d.loc[k] = _values.loc[:,k].to_numpy()[0]
                del _values
                gc.collect()
            i += 1
            if i > 3:
                break
        return d

    def __reorder_columns(self, columns:list, fixed:list) -> list:
        """
        This function is required to reorganize the dataframe according to the requirements 
        posed by the processing function, which requires the fixed effects to be prior to the 
        random effects columns.
        """
        i = 0
        for value in fixed:
            pattern_fixed = re.compile(r"%s[\_0-9]*"%re.escape(value))
            # this was the easiest way to access the variable inside the regular expression
            # usage inspired by:
            # https://stackoverflow.com/questions/6930982/how-to-use-a-variable-inside-a-regular-expression/6931070#6931070
            # Since the original solution of concatenating several strings proved to fail, another
            # solution was taken into consideration, which now uses traditional string formatting. 
            # The solution has been derived from the following comment:
            # https://stackoverflow.com/questions/5900683/using-variables-in-python-regular-expression/5900723#5900723
            try:
                fixed_cols = list(filter(pattern_fixed.match, columns))
            except TypeError:
                continue
            #fixed_cols = fixed + fixed_cols
            for _value in fixed_cols:
                try:
                    columns.insert(
                        i, 
                        columns.pop(
                            columns.index(_value)
                        )
                    )
                    """
                    columns.pop() returns the list value at the given index and removes this respective 
                    value from the list. This results in a list, which is later used as list refering 
                    the columns of the dataframe.
                    """
                    i += 1
                except ValueError:
                    """
                    This exception is mainly caused by named but missing index columns
                    """
                    continue
        return columns

    def calculate_fertility_data(self, data:pd.DataFrame) -> pd.DataFrame:
        """
        This function calculates fertility data, such as gestation length and most likely insemination
        date from the weighed averages for gestation lenght in the data set. The calculation relies on 
        the difference between the calves' sexes when it comes to the gestation length. Therefore, 
        the calf's sex is taken as a primary source of information for the gestation length.
        """
        r = pd.DataFrame()
        for year, _data in data.groupby(data.index.get_level_values(1).year):
            _mean_all = np.ceil(_data.loc[:,"TRAECHTIGKEITSDAUER"].mean())
            _mean_male = np.ceil(
                _data.loc[
                    (_data["TRAECHTIGKEITSDAUER"].notnull()) & 
                    (_data["GESCHLECHT_KALB"].notnull()) &
                    (_data["GESCHLECHT_KALB"] == 1), "TRAECHTIGKEITSDAUER"
                ].mean()
            )
            _mean_female = np.ceil(
                _data.loc[
                    (_data["TRAECHTIGKEITSDAUER"].notnull()) & 
                    (_data["GESCHLECHT_KALB"].notnull()) &
                    (_data["GESCHLECHT_KALB"] == 2), "TRAECHTIGKEITSDAUER"
                ].mean()
            )
            _data.loc[
                (_data["TRAECHTIGKEITSDAUER"].isna()) & 
                (_data["GESCHLECHT_KALB"].notnull()) &
                (_data["GESCHLECHT_KALB"] == 1),
                "TRAECHTIGKEITSDAUER"] = _mean_male
            _data.loc[
                (_data["TRAECHTIGKEITSDAUER"].isna()) & 
                (_data["GESCHLECHT_KALB"].notnull()) &
                (_data["GESCHLECHT_KALB"] == 2),
                "TRAECHTIGKEITSDAUER"] = _mean_female
            _data.loc[
                (_data["TRAECHTIGKEITSDAUER"].isna()) & 
                (_data["GESCHLECHT_KALB"].isna()),
                "TRAECHTIGKEITSDAUER"] = _mean_all
            _data.loc[
                (_data["BELEGDATUM_FUER_KALB"].isna()) & 
                (_data["GESCHLECHT_KALB"].notnull()) & 
                (_data["GESCHLECHT_KALB"] == 1),
                "BELEGDATUM_FUER_KALB"] = _data.loc[
                    (_data["BELEGDATUM_FUER_KALB"].isna()) & 
                    (_data["GESCHLECHT_KALB"].notnull()) &
                    (_data["GESCHLECHT_KALB"] == 1),:
                    ].index.get_level_values(1) - pd.Timedelta(_mean_male, "D")
            _data.loc[
                (_data["BELEGDATUM_FUER_KALB"].isna()) & 
                (_data["GESCHLECHT_KALB"].notnull()) & 
                (_data["GESCHLECHT_KALB"] == 2),
                "BELEGDATUM_FUER_KALB"] = _data.loc[
                    (_data["BELEGDATUM_FUER_KALB"].isna()) & 
                    (_data["GESCHLECHT_KALB"].notnull()) &
                    (_data["GESCHLECHT_KALB"] == 2),:
                    ].index.get_level_values(1) - pd.Timedelta(_mean_female, "D")
            _data.loc[
                (_data["BELEGDATUM_FUER_KALB"].isna()) & 
                (_data["GESCHLECHT_KALB"].isna()),
                "BELEGDATUM_FUER_KALB"] = _data.loc[
                    (_data["BELEGDATUM_FUER_KALB"].isna()) & 
                    (_data["GESCHLECHT_KALB"].isna()),:
                    ].index.get_level_values(1) - pd.Timedelta(_mean_all, "D")
            r = pd.concat([r, _data], axis=0)
        r.loc[
            (r["GUESTZEIT"].isna()) & 
            (r["VORHERIGE_KALBUNG"].notnull()) & 
            (r["BELEGDATUM_FUER_KALB"].notnull()), "GUESTZEIT"] = (r.loc[
                (r["GUESTZEIT"].isna()) & 
                (r["VORHERIGE_KALBUNG"].notnull()) & 
                (r["BELEGDATUM_FUER_KALB"].notnull()),"BELEGDATUM_FUER_KALB"] - r.loc[
                    (r["GUESTZEIT"].isna()) & 
                    (r["VORHERIGE_KALBUNG"].notnull()) & 
                    (r["BELEGDATUM_FUER_KALB"].notnull()),
                    "VORHERIGE_KALBUNG"])/pd.Timedelta(1,"D")
        r["GUESTZEIT"] = r["GUESTZEIT"].astype(np.float)
        r.loc[(r["GUESTZEIT"] < 0) & r["VORHERIGE_KALBUNG"].notnull(), ["GUESTZEIT", "VORHERIGE_KALBUNG"]] = 0
        """
        The whole function is a bunch of advanced indexing to calculate means for the gestation 
        lengths of dairy cattle. Since there is a difference in the gestation length between 
        female and male calves, the calf's sex is taken into consideration. Furthermore, yearly 
        averages in gestation lengths are also included, to account for yearly differences in gestation 
        lengths. Therefore, the function is run in a groupby loop.
        Furthermore, the time period between previous parturition and the insemination leading to 
        the current calf is calculated. This period, which is referred to in the German term Güstzeit,
        is calculated for the previous lactation period, which terminates with the new calf, 
        which is the current calf.
        """
        return r

    def __priors(self, data:pd.DataFrame, columns:list, random_effects:list) -> tuple:
        animal_covar = data.loc[:,columns].cov().round(2)
        animal_covar.index = pd.MultiIndex.from_arrays(
            [
                np.repeat("Covariance", animal_covar.index.size),
                np.repeat("Animal", animal_covar.index.size), 
                animal_covar.index
            ],
            names=["Type","Effect", "Trait"]
        )
        covariance = pd.DataFrame()
        correlation = pd.DataFrame()
        covariance = pd.concat([covariance, animal_covar], axis=0)
        animal_correlation = data.loc[:,columns].corr()
        animal_correlation.index = pd.MultiIndex.from_arrays(
            [
                np.repeat("Correlation", animal_correlation.index.size),
                np.repeat("Animal", animal_correlation.index.size), 
                animal_correlation.index
            ],
            names=["Type", "Effect", "Trait"]
        )
        correlation = pd.concat([correlation, animal_correlation], axis=0)
        for effect in random_effects:
            if effect in data.columns:
                _random_cov = data.loc[
                    :,columns
                ].groupby(data[effect]).cov()
                _random_cov_means = _random_cov.groupby(
                    _random_cov.index.get_level_values(1)
                ).mean()
                _random_cov_means.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat("Covariance", _random_cov_means.index.size),
                        np.repeat(effect, _random_cov_means.index.size), 
                        _random_cov_means.index
                    ], names=["Type", "Effect", "Trait"]
                )
                _random_cov_means.dropna(how="all", axis=1, inplace=True)
                covariance = pd.concat([covariance, _random_cov_means], axis=0)
                del _random_cov, _random_cov_means

                _random_corr = data.loc[
                    :,columns
                ].groupby(data[effect]).corr()
                _random_corr_means = _random_corr.groupby(
                    _random_corr.index.get_level_values(1)
                ).mean()
                _random_corr_means.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat("Correlation", _random_corr_means.index.size),
                        np.repeat(effect, _random_corr_means.index.size), 
                        _random_corr_means.index
                    ], names=["Type", "Effect", "Trait"]
                )
                correlation = pd.concat([correlation, _random_corr_means], axis=0)
                del _random_corr, _random_corr_means
                gc.collect()
        return covariance, correlation

    def __reorg_whole_lactation_data(self, data:pd.DataFrame, **kwargs) -> dict:
        requiring_columns = [
            'VERZOEGERUNGSZEIT', 
            'RASTZEIT', 
            "ERSTE_BELEGUNG", 
            "TRAECHTIGKEITSDAUER", 
            "GESCHLECHT_KALB", 
            "GUESTZEIT",
            "BELEGDATUM_FUER_KALB"
        ]
        if any(x in data.columns for x in requiring_columns) is True:
            data = self.calculate_fertility_data(data)
        _data = self.data_cleaning({f"{kwargs['breed']} 305TL":data})
        if _data is None:
            _data = data
        """
        This is the step to exclude unlikely data, in case this step is not executed, due 
        to the parameters deemed uncleanable, the values passed to the function are taken as 
        the required input and processed in the following steps.
        """
        if len(_data.index) > 0 and "ear_tag_correction" in kwargs.keys():
            _res = self.__feature_selection(
                _data, 
                correct_eartags=kwargs["ear_tag_correction"], 
                single=kwargs["single"],
                columns=kwargs["columns"]
            )
        elif len(_data.index) > 0 and "ear_tag_correction" not in kwargs.keys():
            _res = self.__feature_selection(
                _data, 
                single=kwargs["single"],
                correct_eartags=False,
                columns=kwargs["columns"]
            )
            """
            Calling this function allows to alter the dataframe to match the requirements for dmu or BlupF90, 
            such as writing data for one animal in multiple columns in the same row. Since the order 
            of columns is already fixed after the previous step, The columns can be used to calculate 
            variance and covariance.
            For test and development purposes, the reordering of data in the dataframe can be switched off,
            therefore ommitting the whole function to select features.
            """
        else:
            return None
        del data, _data
        gc.collect()
        priors = pd.DataFrame()
        """
        This column order follows the dmu documentation on pages 19 and 20, where it is 
        stated that this order of designators is required for the priors.
        """
        if "year_season" in kwargs.keys() and kwargs["year_season"] is True:
            try:
                _res["Season"] = _res.index.get_level_values(1).month
                _res["Year"] = _res.index.get_level_values(1).year
            except IndexError:
                i = 1
                for part_col in [x for x in _res.columns if "_".join(x.split("_")[:2]) in "Parturition_date"]:
                    if i == 1:
                        _res["Season"] = pd.to_datetime(_res[part_col], errors='coerce').dt.month
                        _res["Year"] = pd.to_datetime(_res[part_col], errors='coerce').dt.year
                    else:
                        _res[f"Season_{i}"] = pd.to_datetime(_res[part_col], errors='coerce').dt.month
                        _res[f"Year_{i}"] = pd.to_datetime(_res[part_col], errors='coerce').dt.year
                    i += 1
            if kwargs["pedigree"] is not None:
                _with_yob = kwargs["pedigree"].loc[kwargs["pedigree"]["Year of Birth"] > 0,:]
                _res = self.ral.apply_groupby(_res, action="Cow_age", pedigree=_with_yob)
                if 0 in _res.columns:
                    _res.drop(0, axis=1, inplace=True)
        if len(_res.columns) > 1:
            if kwargs["random_effects"] is None:
                covariance = _res[kwargs["prior_columns"]].cov().round(2)
                covariance.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat("Covariance", covariance.index.size),
                        np.repeat("Animal", covariance.index.size), 
                        covariance.index
                    ],
                    names=["Type", "Effect", "Trait"]
                )
                # In this case, there's no need to create a sophisticated index, therefore, the condition in this case 
                # works like this. 
                # The MultiIndex created only serves the purpose of compatibilty with other dataframe objects.
                correlation = _res.loc[:,kwargs["prior_columns"]].corr()
                correlation.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat("Correlation", correlation.index.size),
                        np.repeat("Animal", correlation.index.size), 
                        correlation.index
                    ],
                    names=["Type", "Effect", "Trait"]
                )
            gc.collect()
            _res = _res[self.__reorder_columns(_res.columns.tolist(), kwargs["fixed"])]
            for _col in _res.columns:
                for _var in kwargs["fixed"]:
                    try:
                        numerical_regex = re.findall(
                            r"^%s[\_0-9]*$"%re.escape(_var), _col
                        )
                    except TypeError:
                        continue
                    if numerical_regex:
                        _res[_col] = _res[_col].astype(np.int64)
                    else:
                        continue
            _res.fillna(0, inplace=True)
            for column in _res.columns:
                if column in self.datetime_columns:
                    _res[column] = pd.to_datetime(_res[column], errors='coerce').dt.date
                    # That was the correct advice to convert the columns to full datetime columns
                    # https://stackoverflow.com/questions/26920871/handling-pandas-dataframe-columns-with-mixed-date-formats/26961891#26961891
                    _res[column].fillna(0, inplace=True)
                    _res[column].replace({pd.NaT:0}, inplace=True)
                    # The advice for this type of condition was given here:
                    # https://stackoverflow.com/questions/43214204/how-do-i-tell-if-a-column-in-a-pandas-dataframe-is-of-type-datetime-how-do-i-te/57187654#57187654
                    #_res[column] = _res[column].astype(np.datetime64).dt.date
                    # This step has been advised here:
                    # https://stackoverflow.com/questions/16176996/keep-only-date-part-when-using-pandas-to-datetime/34277514#34277514
                else:
                    try:
                        _res[column] = _res[column].astype(np.float64)
                    except TypeError:
                        continue
            if kwargs["random_effects"] is not None and kwargs["testday"] is None or kwargs["random_effects"] is not None and "testday" not in kwargs.keys():
                covariance, correlation = self.__priors(
                    data=_res, 
                    columns=kwargs["prior_columns"], 
                    random_effects=kwargs["random_effects"]
                )
            if "lactation_model" in kwargs.keys() and "test_day_records" in kwargs.keys():
                _res = self.ral.apply_groupby(
                    _res, 
                    action="Lactation_curve", 
                    lactation=kwargs["lactation_model"],
                    test_days=kwargs["test_day_records"]
                )
                pattern_lact_day = re.compile(r"[0-9]+")
                lact_data = _res.loc[:,[c for c in _res.columns if pattern_lact_day.match(str(c))]]
                lact_data.fillna(0, inplace=True)
                _res.drop(
                    [_c for _c in _res.columns if pattern_lact_day.match(str(_c))], 
                    axis=1, 
                    inplace=True
                )
                _res = pd.concat([_res, lact_data], axis=1)
                if 0 in _res.columns:
                    _res.drop(0, axis=1, inplace=True)
                del lact_data
                gc.collect()
            if "testday" in kwargs.keys() and "test_day_records" in kwargs.keys() and kwargs["testday"] is not None:
                for trait in kwargs["testday"]:
                    if trait in kwargs["test_day_records"]:
                        _res = self.ral.apply_groupby(
                            _res,
                            action="Daily_yields",
                            test_days=kwargs["test_day_records"],
                            trait=trait
                        )
                _res.dropna(thresh=np.ceil(_res.index.size/2), axis=1, inplace=True)
                if kwargs["random_effects"] is not None:
                    columns = kwargs["prior_columns"]
                    tday_cols = []
                    for trait in kwargs["testday"]:
                        tday_pattern = re.compile(r"^%s_\d+$"%re.escape(trait))
                        _tday_cols = [x for x in _res.columns if tday_pattern.match(str(x))]
                        tday_cols = tday_cols + _tday_cols
                    _columns = columns + tday_cols
                    covariance, correlation = self.__priors(
                        data=_res,
                        columns=_columns,
                        random_effects=kwargs["random_effects"]
                    )
            if kwargs["write"] is True:
                _res.fillna(0, inplace=True)
                _res.to_csv(
                    f"{self.pp.milkrecording_folder}/{kwargs['breed']}305TL", 
                    sep=" ",
                    header=True,
                    index=True
                )
                _res.to_csv(
                    f"{self.pp.milkrecording_folder}/{kwargs['breed']} 305TL.csv", 
                    header=True,
                    index=True
                )
                if kwargs["random_effects"] is not None:
                    for _effect in np.unique(covariance.index.get_level_values(1)):
                        effect_priors = covariance.loc[pd.IndexSlice["Covariance", _effect,:],:].dropna(how="all", axis=1)
                        effect_correlation = correlation.loc[
                            pd.IndexSlice["Correlation", _effect,:],:].dropna(how="all", axis=1)
                        effect_correlation.reset_index(inplace=True, drop=False)
                        effect_correlation.index = effect_correlation["Trait"]
                        effect_correlation.drop(["Type", "Effect", "Trait"], axis=1, inplace=True)
                        effect_priors.reset_index(inplace=True, drop=False)
                        effect_priors.index = effect_priors["Trait"]
                        effect_priors.drop(["Type", "Effect", "Trait"], axis=1, inplace=True)
                        if "Animal" in _effect:
                            effect_priors.to_csv(
                                f"{self.pp.milkrecording_folder}/{kwargs['breed']}Priors", 
                                sep=" ",
                                header=False,
                                index=False
                            )
                            effect_priors.to_csv(
                                f"{self.pp.milkrecording_folder}/{kwargs['breed']}Priors_labelled.csv",
                                header=True,
                                index=True
                            )
                            effect_correlation.to_csv(
                                f"{self.pp.milkrecording_folder}/{kwargs['breed']}Correlations",
                                sep=" ",
                                header=False,
                                index=False
                            )
                            effect_correlation.to_csv(
                                f"{self.pp.milkrecording_folder}/{kwargs['breed']}Correlations_labelled.csv",
                                header=True,
                                index=True
                            )
                        else:
                            effect_priors.to_csv(
                                f"{self.pp.milkrecording_folder}/{kwargs['breed']}Priors_{_effect}", 
                                sep=" ",
                                header=False,
                                index=False
                            )
                            effect_priors.to_csv(
                                f"{self.pp.milkrecording_folder}/{kwargs['breed']}Priors_{_effect}_labelled.csv",
                                header=True,
                                index=True
                            )
                            effect_correlation.to_csv(
                                f"{self.pp.milkrecording_folder}/{kwargs['breed']}Correlations_{_effect}",
                                sep=" ",
                                header=False,
                                index=False
                            )
                            effect_correlation.to_csv(
                                f"{self.pp.milkrecording_folder}/{kwargs['breed']}Correlations_{_effect}_labelled.csv",
                                header=True,
                                index=True
                            )
                else:
                    covariance.reset_index(inplace=True, drop=False)
                    covariance.index = covariance["Trait"]
                    covariance.drop(["Type", "Effect", "Trait"], axis=1, inplace=True)
                    covariance.to_csv(
                        f"{self.pp.milkrecording_folder}/{kwargs['breed']}Priors", 
                        sep=" ",
                        header=False,
                        index=False
                    )
                    covariance.to_csv(
                        f"{self.pp.milkrecording_folder}/{kwargs['breed']}Priors_labelled.csv",
                        header=True,
                        index=True
                    )
                    correlation.reset_index(inplace=True, drop=False)
                    correlation.index = correlation["Trait"]
                    correlation.drop(["Type", "Effect", "Trait"], axis=1, inplace=True)
                    correlation.to_csv(
                        f"{self.pp.milkrecording_folder}/{kwargs['breed']}Correlations",
                        sep=" ",
                        header=False,
                        index=False
                    )
                    correlation.to_csv(
                        f"{self.pp.milkrecording_folder}/{kwargs['breed']}Correlations_labelled.csv",
                        header=True,
                        index=True
                    )
            r = {}
            r["data"] = _res
            if kwargs["random_effects"] is not None:
                covariance.reset_index(drop=False, inplace=True)
                covariance.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat("covariance", covariance.index.size),
                        covariance["Effect"],
                        covariance["Trait"]
                    ], names=["Type", "Effect", "Trait"]
                )
                covariance.drop(["Effect", "Trait"], axis=1, inplace=True)
                correlation.reset_index(drop=False, inplace=True)
                correlation.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat("correlation", correlation.index.size),
                        correlation["Effect"],
                        correlation["Trait"]
                    ], names=["Type", "Effect", "Trait"]
                )
                correlation.drop(["Effect", "Trait"], axis=1, inplace=True)
                priors = pd.concat([covariance, correlation], axis=0)
                del correlation, covariance
                gc.collect()
            else:
                priors = pd.concat([covariance, correlation], axis=0)
                del covariance, correlation
            gc.collect()
            r["priors"] = priors
            return r
        else:
            # In case the resulting dataframe remains empty, it is not added to the dictionary
            # and therefore no longer processed.
            return None
    
    def __test_day_records(self, data:pd.DataFrame, **kwargs) -> tuple:
        if "PROBE_DATUM" in data.columns:
            td = data["PROBE_DATUM"] - data.index.get_level_values("Parturition_date")
            if "year_season" in kwargs.keys() and kwargs["year_season"] is True:
                data["Year"] = data["PROBE_DATUM"].dt.year
                data["Season"] = data["PROBE_DATUM"].dt.month
            data["LAKT_TAG"] = (td/np.timedelta64(1, "D")).astype(np.int)
            data["LOG_LAKT_TAG"] = np.log10(data["LAKT_TAG"])
            data.index = pd.MultiIndex.from_arrays(
                [
                    data.index.get_level_values(0), 
                    data.index.get_level_values(1),
                    data["PROBE_DATUM"]
                ], 
                names=["ISO_LEBENSNR", "Parturition_date", "Test_day"]
            )
            data = data[~data.index.duplicated(keep="first")]
            data["TEST_TAG"] = self.ral.apply_groupby(data, action="Test_day")
        covariance = pd.DataFrame(
            columns=np.unique(data.columns), 
            index=np.unique(data.columns)
        )
        for column in data.columns:
            for column2 in data.columns:
                if column == column2:
                    try:
                        covariance.loc[column, column2] = data[column].var()
                    except TypeError:
                        continue
                else:
                    try:
                        _cov = data[[column, column2]].astype(np.float64).cov()
                        covariance.loc[column, column2] = _cov.loc[column, column2]
                        del _cov
                    except TypeError:
                        continue
        correlation = data.corr()
        data.fillna(0, inplace=True)
        if "PROBE_DATUM" in data.columns:
            data.drop("PROBE_DATUM", axis=1, inplace=True)
        if "write" in kwargs.keys() and kwargs["write"] is True:
            data.to_csv(
                f"{self.pp.milkrecording_folder}/{kwargs['breed']}pmergebnisse",
                sep=" ",
                header=True,
                index=True
            )
            data.to_csv(
                f"{self.pp.milkrecording_folder}/{kwargs['breed']}pmergebnisse.csv",
                header=True,
                index=True
            )
            covariance.to_csv(
                f"{self.pp.milkrecording_folder}/{kwargs['breed']}pmergebnisse_priors.csv",
                sep=",",
                index=True,
                header=True
            )
            correlation.to_csv(
                f"{self.pp.milkrecording_folder}/{kwargs['breed']}pmergebnisse_corr.csv",
                sep=",",
                index=True,
                header=True
            )
        correlation.index = pd.MultiIndex.from_arrays(
            [
                np.repeat("Correlation",correlation.index.size), 
                correlation.index
            ], names=["Type", "Trait"]
        )
        covariance.index = pd.MultiIndex.from_arrays(
            [
                np.repeat("Covariance",covariance.index.size), 
                covariance.index
            ], names=["Type", "Trait"]
        )
        priors = pd.concat([covariance, correlation], axis=0)
        del correlation, covariance
        gc.collect()
        return data, priors

    def __data_adaption(self, data:pd.DataFrame, **kwargs):
        all_columns = kwargs["required_columns"] + kwargs["fixed"]
        requiring_columns = [
            'VERZOEGERUNGSZEIT',
            'RASTZEIT',
            'GUESTZEIT'
        ]
        if any(x in all_columns for x in requiring_columns) is True:
            all_columns.extend(
                [
                    "ERSTE_BELEGUNG", 
                    "TRAECHTIGKEITSDAUER", 
                    "GESCHLECHT_KALB", 
                    "GUESTZEIT", 
                    "BELEGDATUM_FUER_KALB",
                    'VORHERIGE_KALBUNG'
                ]
            )
        all_columns = np.unique(all_columns)

        if "ear_tag_correction" in kwargs.keys() and kwargs["ear_tag_correction"] is True:
            data.index = data.index.set_names(["ISO_LEBENSNR"])
            if "ISO_LEBENSNR" not in data.columns:
                data.reset_index(inplace=True, drop=False)
            _data = self.pedigree.eartag_correction(data, "milk_recording")
        else:
            _data = data
            if "KALBEDATUM" in _data.columns:
                _data.index = pd.MultiIndex.from_arrays(
                    [_data.index, _data["KALBEDATUM"]],
                    names=["ear_tag", "parturition_date"]
                )
        if "ear_tag_correction" in kwargs.keys() and kwargs["ear_tag_correction"] is True and "KALBEDATUM" in _data.columns:
            _data.index = pd.MultiIndex.from_arrays(
                    [_data["ISO_LEBENSNR modified"], _data["KALBEDATUM"]],
                    names=["ear_tag", "parturition_date"]
                )
            # The index is only changed after the values in the table have been checked for their 
            # consistency, since the check function works better with a numeric index
        _res = pd.DataFrame()
        prior_columns = []
        for value in all_columns:
            for col in _data.columns:
                pattern = re.compile(
                    r"%s[\_\ A-Za-z0-9]*"%re.escape(value.upper())
                )
                if "ear_tag" in value:
                    continue
                if "TRAECHTIGKEITSDAUER" in value and "TRAECHTIGKEITSDAUER" in col and not re.findall(pattern, col):
                    _res = pd.concat([_res, _data["TRAECHTIGKEITSDAUER"]], axis=1)
                if re.findall(pattern, col):
                    for _x in re.findall(pattern, col):
                        _vars = _data.loc[:,_x]
                        _res = pd.concat([_res, _vars], axis=1)
                        del _vars
                        gc.collect()
        for _col in kwargs["required_columns"]:
            _pattern = re.compile(
                r"%s[\_\ A-Za-z0-9]*"%re.escape(_col.upper())
            )
            for matching in list(filter(_pattern.match, _res.columns)):
                prior_columns.append(matching)
        if "pmergebnisse" in kwargs["tag"] and "PROBE_DATUM" in _data.columns:
            _res = pd.concat(
                [_res, _data.loc[:,"PROBE_DATUM"]], 
                axis=1
            )
        _res.index = pd.MultiIndex.from_tuples(
            _res.index, 
            names=["ISO_LEBENSNR", "Parturition_date"]
        )
        if _res.columns.size > 0:
            return {"data":_res,"all_columns":all_columns,"prior_columns":prior_columns}
        else:
            return None
    
    def adapt_milk_recording(self, required_columns:list, fixed=None, **kwargs) -> dict:
        """
        fixed is an optional parameter, which is only required, when running the 
        processing of 305TL lactation records. Similar to the required_columns paramter, 
        it takes a list as input.
        The overall focus of this function is to adapt the input data for the 
        needs of further analyses, such as with BLUPF90 or dmu. This includes 
        exclusion of unlikely data and calculation of data values, such as gestation 
        length and insemination date.
        """
        if "single" in kwargs.keys():
            single = kwargs["single"]
        else:
            single = False
        if "year_season" in kwargs.keys() and kwargs["year_season"] is True:
            ys = True
            pedigree = self.pedigree.formatForBlupF90(save=False)
        else:
            ys = False
            pedigree = None
        if "test" in kwargs.keys():
            test = kwargs["test"]
        else:
            test = False
        if "write" in kwargs.keys():
            write = kwargs["write"]
        else:
            write = False
        if "save_memory" in kwargs.keys() and kwargs["save_memory"] is True: 
            if "write" not in kwargs.keys() or "write" in kwargs.keys() and kwargs["write"] is False:
                write = True
        if "correct_eartags" in kwargs.keys():
            ear_tag_correction = kwargs["correct_eartags"]
        else:
            ear_tag_correction = False
        if "testday" in kwargs.keys() and kwargs["testday"] is not None:
            testday_traits = kwargs["testday"]
        else:
            testday_traits = None
        if "random_effects" in kwargs.keys() and len(kwargs["random_effects"]) > 0:
            random_effects = kwargs["random_effects"]
        else:
            random_effects = None
        if not isinstance(required_columns, list) and len(required_columns) < 1:
            print(f"Required columns have to be of type list! They are: {type(required_columns)}")
            return None
        if "tl305" in kwargs.keys() and kwargs["tl305"] is True or "lact_model" in kwargs.keys() and kwargs["lact_model"] is True:
            if not isinstance(fixed, list) and len(fixed) < 1:
                print(f"Fixed columns values have to be of type list! They are: {type(fixed)}")
                return None
        _in = self.pp.read_milkrecording(test=test)
        res = {}
        self.datetime_columns = _in["305TL datetime"]
        if "lact_model" in kwargs.keys() and kwargs["lact_model"] is True or "testday" in kwargs.keys() and kwargs["testday"] is not None:
            breed_pattern = re.compile(r"^[A-Z]{3}")
            breeds = list(
                set([x.split(" ")[0] for x in list(filter(breed_pattern.match, _in.keys()))])
            )
            lactation_model = LactationCurves.legendre()
            for breed in breeds:
                testday_data = self.__data_adaption(
                    _in[f"{breed} pmergebnisse"],
                    fixed=fixed,
                    required_columns=required_columns,
                    tag=f"{breed} pmergebnisse",
                    ear_tag_correction=ear_tag_correction
                )
                testday_records, testday_priors = self.__test_day_records(
                    data=testday_data["data"],
                    write=write,
                    breed=breed
                )
                whole_lactation_data = self.__data_adaption(
                    _in[f"{breed} 305TL"],
                    fixed=fixed,
                    required_columns=required_columns,
                    tag=f"{breed} 305TL",
                    ear_tag_correction=ear_tag_correction
                )
                if pedigree is not None:
                    _pedigree = pedigree[breed][0]
                else:
                    _pedigree = None
                    """
                    Beware this indention! The conditions are supposed to include year of 
                    birth information for the animals involved.
                    """
                lactation_records = self.__reorg_whole_lactation_data(
                    data=whole_lactation_data["data"],
                    breed=breed, 
                    ear_tag_correction=ear_tag_correction,
                    write=write,
                    single=single,
                    fixed=fixed,
                    year_season=ys,
                    columns=whole_lactation_data["all_columns"],
                    prior_columns=whole_lactation_data["prior_columns"],
                    random_effects=random_effects,
                    pedigree=_pedigree,
                    lactation_model=lactation_model,
                    test_day_records=testday_records,
                    testday=testday_traits
                )
                if "save_memory" not in kwargs.keys() or "save_memory" in kwargs.keys() and kwargs["save_memory"] is False:
                    res[f"{breed} pmergebnisse"] = testday_records
                    res[f"{breed} pmergebnisse priors"] = testday_priors
                    res[f"{breed} 305TL"] = lactation_records
                else:
                    del testday_data, testday_records, testday_priors, whole_lactation_data, lactation_records
                    gc.collect()
        else:
            for tag, file in _in.items():
                try:
                    breed = re.findall(re.compile(r"^[A-Z]{3}"), tag)[0]
                except IndexError:
                    continue
                _res = self.__data_adaption(
                    file, 
                    fixed=fixed, 
                    required_columns=required_columns, 
                    tag=tag,
                    ear_tag_correction=ear_tag_correction
                )
                if _res is None:
                    continue
                if pedigree is not None:
                    breed_pedigree = pedigree[breed][0]
                else:
                    breed_pedigree = None
                if "305TL" in tag.split()[1]:
                    if "tl305" in kwargs.keys() and kwargs["tl305"] is True:
                        whole_lactations = self.__reorg_whole_lactation_data(
                            data=_res["data"], 
                            breed=breed, 
                            ear_tag_correction=ear_tag_correction,
                            write=write,
                            single=single,
                            fixed=fixed,
                            year_season=ys,
                            columns=_res["all_columns"],
                            prior_columns=_res["prior_columns"],
                            random_effects=random_effects,
                            pedigree=breed_pedigree
                        )
                        if whole_lactations is None:
                            return None
                        if "save_memory" in kwargs.keys() and kwargs["save_memory"] is True and write is True:
                            del whole_lactations
                            gc.collect()
                        else:
                            res[tag] = whole_lactations["data"]
                            res[f"{tag}_prior"] = whole_lactations["priors"]
                elif "pmergebnisse" in tag.split()[1]:
                    if "pmonly" in kwargs.keys() and kwargs["pmonly"] is True:
                        if "save_memory" in kwargs.keys() and kwargs["save_memory"] is True and write is True:
                            self.__test_day_records(
                                data=_res["data"], 
                                write=write,
                                breed=breed
                            )
                        else:
                            res[tag], res[f"{tag}_priors"] = self.__test_day_records(
                                data=_res["data"], 
                                write=write,
                                breed=breed
                            )
        if "save_memory" not in kwargs.keys() or "save_memory" in kwargs.keys() and kwargs["save_memory"] is False:
            return res
