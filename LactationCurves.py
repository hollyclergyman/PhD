#!/usr/bin/env python3

import pandas as pd
import numpy as np

class LactationCurves():
    def legendre(dryoff=305) -> pd.DataFrame:
        r = pd.DataFrame(
            index=np.arange(dryoff + 1,1), 
            columns=["Coefficient", "Percentage", "Absolute Coefficients"]
        )
        for day in range(0, dryoff + 1, 1):
            l = []
            w = 2*((dryoff - (day - 5))/(dryoff - 5)) - 1
            """
            w is the normalized time, which is included in both, the Ali-Schaeffer function and the 
            cubic-legendre model to describe lactation curves. Please refer to Schaeffer, 2004 and 
            Silvestre et al., 2006.
            Since Legendre Polynomials are only defined for value ranges from 0 to 1, all values 
            have to be normalized to match the outward requirements. This relates to the polynomial 
            definition deployed here, which effectively is an implementation of Rodrigues' formula.
            """
            l.append(
                -1 * np.sqrt(
                    (2 * 2 + 1)/2
                )*(0.5 * (3 * w**2 - 1))
            )
            # Legendre 2
            l.append(
                -1 * np.sqrt(
                    (2 * 3 + 1)/2
                )*(0.5*(5*w**3 - 3*w))
            )
            # Legendre 3
            l.append(
                -1 * np.sqrt(
                    (2 * 4 + 1)/2
                ) * ((1/8) * (35 * w**4 - 30 * w**2 + 3))
            )
            # Legendre 4
            #l.append(np.sqrt((2 * 5 + 1)/2)*(1/8) * (63 * w**5 - 70 * w**3 + 15 * w) * np.sqrt((2 * 5 + 1)/2))
            # Legendre 5
            """
            The value for alpha, which is a factor to all polynomes multiplied with psi
            has been chosen with -1 for all polynomes, since the results seem to provide the best
            fit.
            Each Legendre Polynomial has an own correction factor psi, which is expressed as the 
            value in the counter of the fraction under the square root, which is multiplied to the 
            polynomial.
            """
            r.loc[day, "Coefficient"] = np.sum(l)
        """
        This is the daily yield function as described by 
        Schaeffer, 2004 in 
        Application of random regression models in animal breeding
        Equation taken over from:
        Silvestre et al., 2006
        The Accuracy of Seven Mathematical Functions in Modeling 
        Dairy Cattle Lactation Curves Based on Test-Day Records 
        From Varying Sample Schemes
        """
        r["Absolute Coefficients"] = r["Coefficient"] + abs(r["Coefficient"].min())
        r["Percentage"] = r["Absolute Coefficients"]/r["Absolute Coefficients"].sum()
        return r