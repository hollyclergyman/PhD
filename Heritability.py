#!/usr/bin/env python3

import pandas as pd
import numpy as np
import numba
from scipy import sparse
from MultiProcessDivision.divide import divide
from PopulationParameters import PopulationParameters
from Phenotype_data_corrections import Phenotype_data_corrections
from inbreeding import Inbreeding, MissingYOB, RelationAnimals
import Relations
import multiprocessing as mp
import functools
import os
import re
import gc


class Heritability():
    def __init__(self):
        self.__pp = PopulationParameters()
        self.__phdc = Phenotype_data_corrections()
        if "/home/users" in os.environ["HOME"]:
            self.write_path = f"{os.environ['HOME']}/Doktorarbeit/Heritability/"
            self.read_path = self.write_path
        else:
            self.write_path = f"{os.environ['HOME']}/Dokumente/Uni/Mitarbeiterstelle Witzenhausen/Heritability/"
            self.read_path = self.write_path

    def pedigree_data_processing(
        self, 
        destination_traits:list,
        depth:int,
        selected_breed=None,
        pmergebnisse=False,
        write=False,
        read=False,
        test=False
        ):
        ped_data = self.__pp.pedigree_data()
        pheno_data = self.__pp.read_milkrecording(test=test)
        pattern = re.compile(r"^Pedigree\ [A-Z]+$")
        relations = pd.DataFrame()
        inbreeding = pd.DataFrame()
        coefficients_of_relation = pd.DataFrame()
        t_matrix = pd.DataFrame()
        phenotypes = pd.DataFrame()
        for key in list(filter(pattern.match, ped_data)):
            pattern_breed = re.compile(r"[A-Z]{2,}")
            breed = re.findall(pattern_breed, key)[0]
            if selected_breed is not None and breed not in selected_breed:
                continue
            _pheno = self.__get_phenotypic_data(
                pheno_data, 
                breed,
                pmergebnisse,
                destination_traits
            )
            if _pheno is not None:
                _pheno.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat(breed, len(_pheno.index)), 
                        _pheno.index
                    ], names=["Breed", "Ear tag"]
                )
                phenotypes = pd.concat([phenotypes, _pheno], axis=0)
            else:
                continue
            if read is False or write is True:
                #pdb.set_trace()
                _data = ped_data[key]
                """
                The following two _data.loc[] statements select all animals, for which the 
                parents are also part of the pedigree supplied to the function. This step 
                is meant to speed up the following relationship creation, since only animals with 
                known pedigree are included in the ongoing process.
                """
                by_dams = _data.loc[
                    (
                        _data["Ear tag dam"].isin(
                            _data.loc[_data["Sex"] == 2,:].index.get_level_values(0)
                        )
                    ),:
                ]
                by_sires = _data.loc[
                    (_data["Ear tag sire"].isin(
                            _data.loc[_data["Sex"] == 1,:].index.get_level_values(0)
                        )
                    ),:
                ]
                data = pd.concat([by_dams, by_sires], axis=0, sort=False).drop_duplicates(keep="first")
                missing_yob = data.loc[
                        data["Year of Birth"] == 0,:
                    ].index.get_level_values(0)
                data = data.loc[
                    :,
                    [
                        "Year of Birth", 
                        "Sex", 
                        "Ear tag sire", 
                        "Year of Birth sire", 
                        "Ear tag dam", 
                        "Year of Birth dam"
                    ]
                ]
                data.reset_index(inplace=True, drop=False)
                data.drop("No. animal", axis=1, inplace=True)
                data.index = data["Ear tag"]
                data.drop("Ear tag", axis=1, inplace=True)
                data = pd.DataFrame(
                    columns = [
                        'Year of Birth', 
                        'Sex', 
                        'Ear tag sire', 
                        'Year of Birth sire',
                        'Ear tag dam', 
                        'Year of Birth dam'
                    ],
                    data=MissingYOB().missingYOB(
                        data.loc[missing_yob,:],
                        data
                    )
                )
                del _data, by_sires, by_dams
                gc.collect()
                
                ped_eartags = np.unique(data.index)
                listed_animals = data.loc[
                    data.index.isin(ped_eartags),:
                ]
                """
                The phenotype dataframe is already processed to allow passing it to subsequent functions. 
                Therefore, it contains the breed in the first index level and the ear tag in the following 
                one. Beware this fact, when reading the previous list comprehension.
                """
                all_animals = np.unique(
                    np.concatenate(
                        [
                            listed_animals.index, 
                            listed_animals["Ear tag sire"].values, 
                            listed_animals["Ear tag dam"].values
                        ]
                    )
                )
                required_animals = data.loc[
                    data.index.isin(all_animals),:]
                splitted_for_relations = [np.array(x.index) for x in divide(required_animals, axis=0)]
                #profile = cProfile.Profile()
                #profile.enable()
                __relations = self.control_for_relations(
                    animals=splitted_for_relations,
                    data=required_animals.to_numpy(),
                    depth=depth,
                    breed=breed,
                    read=read,
                    write=write
                )
                """
                Since the main difference between the dataframe used for inbreeding calculation and the 
                original dataframe is its index, the dataframe for inbreeding calculation is caught in 
                the reset of the original's dataframe index.
                To avoid errors of unspecific selection statements in later stages, duplicates are ultimately
                dropped in this stage, to minimize on exception handling efforts when running the following 
                numba.jit() decorated functions.
                """
                data_for_inbreeding = data[
                    ~data.index.duplicated(keep="first")
                ]
                data_for_inbreeding.loc[
                    (data_for_inbreeding["Ear tag sire"] == 0) & 
                    (data_for_inbreeding["Year of Birth"] != 0), "Year of Birth"] = data_for_inbreeding.loc[
                        (data_for_inbreeding["Ear tag sire"] == 0) & 
                        (data_for_inbreeding["Year of Birth"] != 0), "Year of Birth"] * -1
                data_for_inbreeding.loc[
                    (data_for_inbreeding["Ear tag dam"] == 0) & 
                    (data_for_inbreeding["Year of Birth"] != 0), "Year of Birth"] = data_for_inbreeding.loc[
                        (data_for_inbreeding["Ear tag dam"] == 0) & 
                        (data_for_inbreeding["Year of Birth"] != 0), "Year of Birth"] * -1
                """
                The inbreeding calculation assumes the year of birth for animals without known parents to be 
                negative. Therefore, all animals, for which either parent is unknown and the year of birth is 
                not 0, receive a year of birth multiplied with -1.
                This step and the following sorting are mentioned in the respective paper by Aguilar & Misztal, 2009.
                """
                data_for_inbreeding = data_for_inbreeding.sort_values(
                    "Year of Birth"
                )
                data_for_inbreeding.fillna(0, inplace=True)
                data_for_inbreeding.reset_index(inplace=True, drop=False)
                """
                This is the step to obtain an index with ascending numbers for the animals 
                involved, with an order from oldest to youngest animal. The dataframe referenced
                with data_for_inbreeding solely exists for the very purpose to get the data in the 
                correct format for inbreeding calculation, which mainly consists of running a second
                duplicate removal and creating an index as described.
                """
                rel_animals = __relations.loc[
                    :,pd.IndexSlice[:,["Sire","Dam"]]].to_numpy().reshape(-1,1)
                relation_animals = np.unique(
                    rel_animals[np.logical_not(np.isnan(rel_animals))]
                ).tolist()
                relation_animals = RelationAnimals().availableAnimals(
                    relation_animals, 
                    np.array(__relations.columns.get_level_values(0))
                )
                data_for_inbreeding = data_for_inbreeding.loc[
                        data_for_inbreeding.index.isin(
                            relation_animals
                        ),:
                    ]
                __inbreeding = self.control_for_inbreeding(
                    animals=data_for_inbreeding,
                    breed=breed,
                    read=read,
                    write=write
                )
                del data_for_inbreeding
                gc.collect()
                
                _coeff_of_relation = self.control_for_coeff_of_relations(
                    data=splitted_for_relations,
                    breed=breed,
                    read=read,
                    write=write
                )
                del splitted_for_relations
                gc.collect()
                                
                splitted_for_tmatrix = divide(
                    phenotypes.loc[pd.IndexSlice[breed,:],:]
                )
                _t_matrix = pd.DataFrame()
                with mp.Pool(processes=mp.cpu_count()) as pool4:
                    to_execute = functools.partial(
                        self.create_t_inv, all_animals
                    )
                    __results = pd.concat(
                        [_resulting for _resulting in pool4.map(
                            to_execute, splitted_for_tmatrix
                        ) if _resulting is not None]
                    )
                    _t_matrix = pd.concat([t_matrix, __results], axis=0)
                    del __results
                    gc.collect()
                del data
                gc.collect()
                """
                The reason for this odd definition of index values in these two pd.DataFrames is 
                that the functions following the definition of these dataframes are not prepared 
                to process multiindex dataframes. Therefore, only the write version of the dataframe
                is adapted to allow API compatibility.
                """
                _t_matrix.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat(breed, len(t_matrix.index)),
                        t_matrix.index
                    ], names=["Breed", "Ear tag"]
                )
                t_matrix = pd.concat([t_matrix, _t_matrix], axis=0)
                __inbreeding.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat(breed, len(__inbreeding.index)),
                        __inbreeding.index
                    ], names=["Breed", "Ear tag"]
                )
                __relations.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat(breed, len(__relations.index)),
                        __relations.index
                    ], names=["Breed", "Ear tag"]
                )
                _coeff_of_relation.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat(breed, len(_coeff_of_relation.index)),
                        _coeff_of_relation.index
                    ], names=["Breed", "Ear tag"]
                )
                relations = pd.concat([relations, __relations], axis=0)
                inbreeding = pd.concat([inbreeding, __inbreeding], axis=0)
                coefficients_of_relation = pd.concat(
                    [coefficients_of_relation, _coeff_of_relation], 
                    axis=0
                )
                #del self.__relations, self.__inbreeding, _coeff_of_relation
                #gc.collect()
            else:
                _coeff_of_relation = pd.read_csv(
                    os.path.join(
                        self.read_path,
                        f"Coefficient of relation {breed}.csv"
                    ),
                    index_col=[0,1],
                    header=0
                )
                coefficients_of_relation = pd.concat(
                    [coefficients_of_relation, _coeff_of_relation], axis=0
                )
                del _coeff_of_relation
                gc.collect()
                __inbreeding = pd.read_csv(
                    os.path.join(
                        self.read_path,
                        f"Inbreeding_{breed}.csv"
                    ), header=0, index_col=[0,1]
                )
                inbreeding = pd.concat([inbreeding, __inbreeding], axis=0)
                del __inbreeding
                gc.collect()
                __relations = pd.read_csv(
                    os.path.join(
                        self.read_path,
                        f"Relations_{breed}.csv"
                    ),
                    index_col=[0,1], header=[0,1]
                )
                relations = pd.concat([relations, __relations], axis=0)
                del __relations
                gc.collect()
        return {
            "Inbreeding":inbreeding,
            "Relations":relations,
            "Coefficients of relation":coefficients_of_relation,
            "Phenotypes": phenotypes
        }

    def __read_data(self, _type:str, breed:str, relations:bool) -> pd.DataFrame:
        if relations is False:
            try:
                _file = pd.read_csv(
                    os.path.join(
                        self.read_path,
                        f"{_type}{breed}.csv"
                    ), header=0, index_col=[0,1]
                )
                _file.reset_index(inplace=True, drop=False)
                _file.index = _file["Ear tag"]
                _file.drop(["Breed", "Ear tag"], axis=1, inplace=True)
                if "Coefficient of relation " in _type:
                    _file.columns = _file.columns.astype(np.int64)
                return _file
            except FileNotFoundError:
                return None
        if relations is True:
            try:
                _file = pd.read_csv(
                    os.path.join(
                        self.read_path, 
                        f"{_type}{breed}.csv"
                    ), header=[0,1], index_col=[0,1]
                )
                _file.reset_index(inplace=True, drop=False)
                _file.index = _file["Generation"]
                _file.drop(["Breed", "Generation"], axis=1, inplace=True)
                _file.columns = pd.MultiIndex.from_arrays(
                    [
                        _file.columns.get_level_values(0).astype(np.int64), 
                        _file.columns.get_level_values(1)
                    ], names=["Ear tag", "Parents"]
                )
                return _file
            except FileNotFoundError:
                return None

    def control_for_inbreeding(
        self, animals:pd.DataFrame, breed:str, read:bool, write:bool) -> pd.DataFrame:
        """
        This is the control function for the multiprocessing scheme to run the inbreeding calculation.
        Similar to all other functions, which serve a similar purpose, this function allows for a 
        condition, whether it reads the data from a file calculated in a previous run or whether it 
        runs the calculation as a multiprocessing loop. A sole difference to other functions is, that 
        it calls a compiled C function and therefore does not include a function definition on variable 
        instantiation.
        """
        if read is True and f"Inbreeding_{breed}.csv" in os.listdir(self.read_path):
            _inbreeding = self.__read_data(
                _type="Inbreeding_", breed=breed, relations=False
            )
            if _inbreeding is not None:
                return _inbreeding
        else:
            _inbreeding = Inbreeding.inbreedingCalculation(animals=animals)
            if write is True:
                _inbreeding.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat(breed, len(_inbreeding.index)),
                        _inbreeding.index
                    ], names=["Breed", "Ear tag"]
                )
                _inbreeding.to_csv(
                    os.path.join(
                        self.write_path, 
                        f"Inbreeding_{breed}.csv"
                    ), header=True, index=True
                )
            return _inbreeding

    def control_for_relations(
        self, animals:list, data:np.ndarray, depth:int, breed:str, read:bool, write:bool
        ) -> pd.DataFrame:
        if read is True and f"Relations_{breed}.csv" in os.listdir(self.read_path):
            _relations = self.__read_data(
                _type="Relations_", breed=breed, relations=True
            )
            if _relations is not None:
                return _relations
        else:
            _relations = pd.DataFrame()
            with mp.Pool(processes=mp.cpu_count()) as pool:
                res = [
                    d for d in pool.starmap(
                        Relations.determine_relations, [(data, depth, _animal) for _animal in animals]
                    ) if d is not None
                ]
            for _dict in res:
                for animal in _dict.keys():
                    _r = pd.DataFrame()
                    sires = pd.Series(data=_dict[animal]["sires"], name="Sire")
                    dams = pd.Series(data=_dict[animal]["dams"], name="Dam")
                    _r = pd.concat([_r, sires, dams], axis=1)
                    _r.columns = pd.MultiIndex.from_arrays(
                        [np.repeat(animal, len(_r.columns)), _r.columns],
                        names=["Ear tag", "Parents"]
                    )
                    _relations = pd.concat([_relations, _r], axis=1)
                    del _r, sires, dams
                    gc.collect()
            if write is True:
                _relations.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat(breed, len(_relations.index)),
                        _relations.index
                    ], names=["Breed", "Generation"]
                )
                _relations.to_csv(
                    os.path.join(
                        self.write_path, 
                        f"Relations_{breed}.csv"
                    ), header=True, index=True
                )
            return _relations

    def control_for_coeff_of_relations(self,data:list,breed:str,read:bool,write:bool) -> pd.DataFrame:
        if read is True and f"Coefficient of relation {breed}.csv" in os.listdir(self.read_path):
            _coeff_of_relation = self.__read_data(
                _type="Coefficient of relation ",breed=breed,relations=False
            )
            if _coeff_of_relation is not None:
                return _coeff_of_relation
        else:
            _coeff_of_relation = pd.DataFrame()
            with mp.Pool(processes=mp.cpu_count()) as pool:
                __res = pd.concat([
                    _x for _x in pool.map(
                        self.coeff_of_relation, data
                    ) if _x is not None
                ], axis=0
                )
                _coeff_of_relation = pd.concat(
                    [_coeff_of_relation, __res], axis=0
                )
                del __res
                gc.collect()
            if write is True:
                _coeff_of_relation.index = pd.MultiIndex.from_arrays(
                    [
                        np.repeat(breed, len(_coeff_of_relation.index)),
                        _coeff_of_relation.index
                    ], names=["Breed", "Ear tag"]
                )
                _coeff_of_relation.to_csv(
                    os.path.join(
                        self.write_path, 
                        f"Coefficient of relation {breed}.csv"
                    ), index=True, header=True
                )
            return _coeff_of_relation

    def __get_phenotypic_data(
        self, 
        pheno_data: dict, 
        breed: str, 
        pmergebnisse: bool, 
        destination_traits: list) -> pd.DataFrame:
        """
        This function is the processing function to parse phenotypic data for the ongoing
        process. It is called rather early, since that allows the processing steps to 
        only include animals, for which phenotypic data is available.
        """
        ped_pattern = re.compile(r"^[A-Z\ pP]+edigree")
        breed_pheno = [
            x for x in pheno_data.keys() if breed in x and not re.match(ped_pattern, x)
        ]
        if breed_pheno:
            _pheno = pd.DataFrame()
            for _pheno_table in breed_pheno:
                if pmergebnisse is False and "pmergebnisse" in _pheno_table:
                    continue
                else:
                    dset = pheno_data[_pheno_table]
                    dset.index = pd.MultiIndex.from_arrays(
                        [dset.index, dset["KALBEDATUM"]],
                        names=["ISO_LEBENSNR", "Parturition_date"]
                    )
                    """
                    This MultiIndex with the naming appended is the index used in the 
                    cleaning function to exclude unlikely traits from further calculation. 
                    Since the function to do data cleaning relies on this naming, it is 
                    necessary to do that in the first step, before passing data to 
                    this function.
                    """
                    enhanced_pheno = self.__phdc.calculate_fertility_data(
                        dset
                    )
                    cleaned = self.__phdc.data_cleaning(
                        enhanced_pheno, breed=breed
                    )
                    for trait in destination_traits:
                        trait_pattern = re.compile(
                            r"%s[\_\ A-Za-z0-9]*"%re.escape(trait.upper())
                        )
                        for col in cleaned.columns:
                            if re.findall(trait_pattern, col):
                                _d = pheno_data[_pheno_table].loc[
                                    :,col
                                ]
                                _pheno = pd.concat([_pheno, _d], axis=1)
            return _pheno

    @numba.jit(forceobj=True)
    def coeff_of_relation(self, data:np.array) -> pd.DataFrame:
        """
        This function is the final function of the raw pedigree data processing group, which is run by 
        pedigree_data_processing(). The purpose of the function is to merge the results of the two previous
        functions, which is the inbreeding coefficient for each animal in the data set and the direct pedigree 
        for each animal.
        To serve the functions purpose, the first step is to find matching animals in the pedigrees of both animals 
        compared. Only in case of common ancestors, the coefficient of relation for the two animals is calculated. 
        This step of common ancestor search is indirectly done by using a Bloom filter, which returns a value, 
        if a match occurs. 
        In the next step, the index values of the matching animals are taken, aswell as the ear tag numbers. 
        As the formula for calculating the coefficient of relationship takes the latest event of common 
        parents into account, the minimum index value for common parents in the dataframe is determined.
        Furthermore, the inbreeding coefficient for each of the parents involved in inbreeding is 
        included to the formula
        """
        r = pd.DataFrame()
        for cattle in data:
            adata = pd.Series(
                index=np.unique(self.__relations.columns.get_level_values(0)), 
                name=cattle
            )
            for comparing_cattle in np.unique(self.__relations.columns.get_level_values(0)):
                if data.loc[:,pd.IndexSlice[cattle,"Dam"]].isin(self.__relations.loc[:,pd.IndexSlice[comparing_cattle,"Dam"]]).any():
                    pos_cattle_dam = data.loc[
                        data.loc[
                            :,
                            pd.IndexSlice[cattle,"Dam"]
                        ].isin(
                            self.__relations.loc[
                                :,
                                pd.IndexSlice[comparing_cattle,"Dam"]
                            ]
                        ) == True]
                    dams = [i for i in pos_cattle_dam.index]
                else:
                    dams = []
                if data.loc[:,pd.IndexSlice[cattle,"Sire"]].isin(self.__relations.loc[:,pd.IndexSlice[comparing_cattle,"Sire"]]).any():
                    pos_cattle_sire = data.loc[
                        data.loc[
                            :,
                            pd.IndexSlice[cattle,"Sire"]
                        ].isin(
                            self.__relations.loc[
                                :,
                                pd.IndexSlice[comparing_cattle,"Sire"]
                            ]
                        ) == True]
                    sires = [_i for _i in pos_cattle_sire.index]
                else:
                    sires = []
                if not sires:
                    generation_sires = 0
                else:
                    generation_sires = min(sires)
                if not dams:
                    generation_dams = 0
                else:
                    generation_dams = min(dams)
                common_predecessors = len(dams) + len(sires)
                adata.loc[comparing_cattle] = np.power(
                    0.5, generation_dams + generation_sires + 1
                    ) * sum(
                        [1 + self.__inbreeding.loc[_x] for _x in common_predecessors]
                    )
            adata = adata.astype(pd.SparseDtype(np.float, np.nan))
        return r
    
    @numba.jit(forceobj=True)
    def create_t_inv(self, included_animals:np.array, data:pd.DataFrame) -> pd.DataFrame:
        """
        The animal model, which is the BLUP model to be created in this class, relies on 
        an upper triangular matrix to refer the genetic merit of the parents to the 
        offspring. Each relationship with known parents is denoted with -0.5, while the diagonal 
        values for the individual animal are set to 1. To avoid double loops in the process, the 
        latter part of the matrix is created as an identity matrix. The parent-offspring relation 
        is created in the next step, by running a single loop.
        This matrix is a little confusing, since the particular cells refer to the offspring of 
        the parents listed in index and the columns of the dataframe. As the labels for both 
        these axes are set to the parents and the animals, for which phenotypic records exist, 
        the animals with records might go amiss, while reading the code. Since the phenotypes 
        measure the parents' genetic merit, which is scaled in the resulting matrix with -0.5 per 
        known parent, by measuring offspring, the offspring is included in the first index level.
        Beware that the data passed to the function is the phenotypes dataframe and NOT the 
        pedigree dataframe. That's caused by the fact that in the following steps only animals 
        with phenotypic data are of relevance. As the breed is already added as a trait to the 
        dataframe's index, the following list comprehension has to account for the  difference in the 
        index.
        """
        t_inv = pd.DataFrame()
        for animal in np.flip(data.index.get_level_values(0)):
            dam = data.loc[pd.IndexSlice[animal,:],"Ear tag dam"]
            sire = data.loc[pd.IndexSlice[animal,:],"Ear tag sire"]
            adata = pd.Series(index=included_animals, name=animal)
            adata.loc[dam] = -0.5
            adata.loc[sire] = -0.5
            adata.loc[animal] = 1
            adata = adata.astype(pd.SparseDtype(np.float, np.nan))
            """
            Now we are in the size area, where it's more efficient to use a sparse matrix to 
            save memory. Since most of the dataframe involved is set to 0, this approach is 
            quite straightforward. The implementation present here relies on the documentation 
            example on how to create a sparse DataFrame. Since it seems more efficient to create 
            the DataFrame from sparse Series elements, this way is chosen.
            """
            t_inv = pd.concat([t_inv, adata], axis=1)
        return t_inv.T

    def create_a_inv(self):
        pass

if __name__ == "__main__":
    Heritability().pedigree_data_processing(
        depth=0,destination_traits=["MILCH_KG", "FETT_KG"], write=True
    )