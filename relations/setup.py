#!/usr/bin/python3 

from setuptools import setup, Extension
from Cython.Build import cythonize
import numpy as np
import os
import shutil
import re

compile_args = [
    "-Wno-unused-result", 
    "-Wsign-compare", 
    "-DNDEBUG", 
    "-fmessage-length=0",
    "-grecord-gcc-switches", 
    "-O2", 
    "-Wall", 
    "-D_FORTIFY_SOURCE=2", 
    "-fstack-protector-strong",
    "-funwind-tables", 
    "-fasynchronous-unwind-tables", 
    "-g", 
    "-DOPENSSL_LOAD_CONF", 
    "-fwrapv", 
    "-fno-semantic-interposition"
]
"""
The compile_args are supposed to generate some level of commoness between the development
system and the server, where the calculation is done. Since the compiler version used, differs between 
the systems, the outcome might prove not to be binary identic.
"""
ext_modules_inbreeding = [
    Extension(
            "Relations",
            ["relations/Relations.pyx"],
            extra_compile_args=compile_args
        )
]
setup(
    name="Relations",
    package_dir={"Relations": ""},
    ext_modules=cythonize(
        ext_modules_inbreeding,
        compiler_directives={'language_level':'3'}
    ), 
    include_dirs=[np.get_include()],
    language_level="3",
    license="BSD",
    author="Thomas Rahimi", 
    author_email="thomas.rahimi@agrar.uni-kassel.de",
    packages=["relations"],
    install_requires=["numpy"],
    version="1.0",
    description="This library creates trees of ancestry between animals of dataset",
    classifiers=[
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: BSD License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6"
    ]
)