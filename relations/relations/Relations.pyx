import numpy as np
cimport numpy as np
np.import_array()
#cython: language_level=3

cpdef dict determine_relations(np.ndarray data, int depth, np.ndarray animals):
    # This function is the control function required to run the following __get_respective_parents()
    # function. The task of this function is to select the appropriate data from the chunked 
    # dataframe, which is argument to the function. As multiprocessing.pool().map() does not take
    # private functions as an argument, this function has to be public, althought it is not intended
    # to be run by a function outside this class.
    # The data passed to the function are those animals and their parents, for which phenotypic records
    # exist, within the range of the program. Therfore, the amount of animals passed to the function 
    # should be around 40,000.
    cdef dict r = {}
    for animal in np.flip(animals):
        adata = {}
        sires = get_respective_parents(
            animal=animal, data=data, sire=1, dam=0, depth=depth
        )
        if sires is not None:
            adata["sires"] = sires
        dams = get_respective_parents(
            animal=animal, data=data, sire=0, dam=1, depth=depth
        )
        if dams is not None:
            adata["dams"] = dams
        r[animal] = adata
    return r

cdef list get_respective_parents(long animal, np.ndarray data, bint sire, bint dam, int depth):
    # The core task of this function is to recursively create descendance schemes for all 
    # animals present in the pedigree table. Since all animals are assigned information on 
    # their parents, these information including ear tag value and year of birth, can be 
    # gathered recursively. 
    # In the previous versions, the recursive gathering of dam and sire to an animal was 
    # always the key problem to be solved for efficient population parameter calculation. 
    # This step has now been solved by using a loop on a python3 list. The list is initiated
    # with the respective parent value for an animal, from where the loop runs backwards until
    # no further parents are assigned. 
    # If only an overview over the population is required, it is possible to limit the function's
    # scope by setting the depth parameter value to an integer, which is the amount of iterations 
    # run by the loop.
    r = []
    if dam == 0 and sire == 1:
        index = 3
    if dam == 1 and sire == 0:
        index = 5
    _animal = np.where(np.vstack((data))[:,0] == animal)[0][0]
    r.append(data[_animal,index])
    for parent in r:
        if depth <= 0:
            try:
                _parent = np.where(np.vstack((data))[:,0] == parent)[0][0]
                r.append(data[_parent,index])
            except IndexError:
                r.append(0)
                break
        else:
            if len(r) < depth:
                try:
                    _parent = np.where(np.vstack((data))[:,0] == parent)[0][0]
                    r.append(data[_parent,index])
                except IndexError:
                    r.append(0)
                    break
    return r
