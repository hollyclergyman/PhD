#!/usr/bin/env python3

import pandas as pd
import numpy as np
import re
import os
import argparse
from PopulationParameters import PopulationParameters


class ProcessForBLUPF90():
    def __init__(self):
        self.__pp = PopulationParameters()
        self.__milkrecording_folder = self.__pp.milkrecording_folder
        if "/home/users" in os.environ["HOME"]:
            self.__blupf90_folder = os.path.join(
                os.environ["HOME"], "Doktorarbeit/BlupF90/"
            )
        else:
            self.__blupf90_folder = os.path.join(
                os.environ["HOME"], 
                "Dokumente/Uni/Mitarbeiterstelle Witzenhausen/BlupF90"
            )

    def parameters(self):
        parser = argparse.ArgumentParser(
            description="Processing phenotypic information to meet the requirements by BLUPF90"
        )
        parser.add_argument("-dst", "--destination", type=str, nargs="+")
        parser.add_argument("-it", "--iteration", type=str, nargs="+")
        parser.add_argument("-f", "--file", type=str, nargs="+")
        return vars(parser.parse_args())
    
    def read_parameters(self):
        arguments = self.parameters()
        param = {}
        for key, value in arguments.items():
            if "destination" in key and os.path.isdir(os.path.join(
                    self.__blupf90_folder, 
                    value
                )):
                param["dest"] = os.path.join(
                    self.__blupf90_folder, 
                    value
                )
            if "iteration" in key:
                param["iteration"] = value
            if "file" in key:
                try:
                    pd.read_csv(os.path.join(self.__milkrecording_folder, value))
                    param["path"] = os.path.join(
                        self.__milkrecording_folder,
                        value
                    )
                    param["file"] = value
                except FileNotFoundError:
                    continue
        return param

    def adapt_file(self):
        params = self.read_parameters()
        if "file" not in params.keys():
            print("Error reading file")
            return None
        else:
            d = pd.read_csv(
                params["path"], 
                sep=" ", 
                header=0, 
                index_col=[0,1]
            )
            if "iteration" in params.keys():
                pattern = re.compile(r"^[A-Z\_0-9]+$")
            else:
                pattern = re.compile(r"^[A-Z]+$")
            d = d.loc[:,list(filter(pattern.match, d.columns))]
            d.replace({0, np.NaN}, inplace=True)
            d.dropna(thresh=np.ceil(len(d.columns)/2), axis=0, inplace=True)
            d.fillna(0, inplace=True)
            d.to_csv(
                os.path.join(params["dest"], params["file"]), 
                sep=" ", 
                index=True, 
                header=True
            )

if __name__ == "__main__":
    ProcessForBLUPF90().adapt_file()
